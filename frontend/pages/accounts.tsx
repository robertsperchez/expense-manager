import AccountsContent from "@components/accounts/accounts-content";
import { Card, Typography } from "@mui/material";

export default function Accounts() {
  return (
    <div
      className="accounts-page"
      style={{
        backgroundImage: "url('../resources/homepage-background.svg')",
        backgroundAttachment: "fixed",
        backgroundSize: "cover",
        minWidth: "100%",
      }}
    >
      <Card
        className="accounts-card"
        sx={{ boxShadow: 4, backgroundColor: "#76d275"}}
      >
        <Typography variant="h4" sx={{ mt: "1%", color: "white" }}>
          Accounts
        </Typography>
        <AccountsContent />
      </Card>
    </div>
  );
}
