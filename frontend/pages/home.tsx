import "../styles/styles.scss";
import laptopHome from "../resources/laptop-homepage.png";
import Button from "@mui/material/Button";
import { useAuth } from "@providers/auth";
import { createRoute } from "../routes/routes";
import { Link } from "react-router-dom";

export default function Home() {
  const auth = useAuth();

  const handleLogin = () => {
    window.location.href = `http://localhost:8080/auth/login?backTo=${encodeURIComponent(
      window.location.href
    )}`;
  };

  return (
    <div
      className="homepage"
      style={{
        backgroundImage: "url('../resources/homepage-background.svg')",
        backgroundAttachment: "fixed",
        backgroundSize: "cover",
        minWidth: "100%",
      }}
    >
      <div className="homepage-content">
        <h1 className="title-text">Create a budget for your everyday needs!</h1>
        <p className="subtitle-text">
          Budgeting isn’t about limiting yourself – it’s about making the things
          that excite you possible. With Sparrow, budgeting becomes easier by
          providing straightforward tools to add your incomes and expenses as
          well as keeping track of the money you have, so that achieving your
          dreams becomes simpler!
        </p>
        {!auth.loggedIn && !auth.loading && (
          <div>
            <p className="subtitle-text">Ready to start? Then log in!</p>
            <div className="homepage-button-container">
              <Button
                variant="contained"
                onClick={handleLogin}
                style={{
                  backgroundColor: "#76d275",
                  width: "150px",
                  fontFamily: "Raleway",
                }}
              >
                Login
              </Button>
            </div>
          </div>
        )}

        {auth.loggedIn && (
          <div>
            <p className="subtitle-text">Ready to start? Go to the dashboard!</p>
            <div className="homepage-button-container">
              <Button
                variant="contained"
                component={Link}
                to={createRoute.dashboard()}
                style={{
                  backgroundColor: "#76d275",
                  width: "150px",
                  fontFamily: "Raleway",
                }}
              >
                Dashboard
              </Button>
            </div>
          </div>
        )}
      </div>

      <div className="homepage-image">
        <img src={laptopHome} className="laptop-home" />
      </div>
    </div>
  );
}
