import { Card, CardContent, Typography, CardMedia } from "@mui/material";

export default function About() {
  return (
    <div
      className="accounts-page"
      style={{
        backgroundImage: "url('../resources/homepage-background.svg')",
        backgroundAttachment: "fixed",
        backgroundSize: "cover",
        minWidth: "100%",
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
      }}
    >
      <Typography
        sx={{
          marginLeft: "auto",
          marginRight: "auto",
          fontFamily: "Raleway",
          marginTop: "1%",
        }}
        variant="h3"
      >
        What can Sparrow do?
      </Typography>
      <Card
        sx={{
          display: "flex",
          width: "80%",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "1%",
        }}
      >
        <CardContent sx={{ flex: "1 0 auto" }}>
          <Typography
            component="div"
            variant="h5"
            sx={{ fontFamily: "Raleway" }}
          >
            See your data in a beautiful way!
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
            width={850}
            sx={{ marginTop: "3%", fontFamily: "Raleway" }}
          >
            An important part of the Sparrow app is the ability to visualize
            your expenses, balances and such in an interactive, beautiful way.
            Wonder how much you've spent on groceries, or how much you've put in
            your car? You can see this in the graphs and charts.
          </Typography>
        </CardContent>
        <CardMedia
          component="img"
          sx={{ width: 300, margin: "1.5% 3% 1.5% 3%" }}
          image="../resources/about-page-graph.png"
          alt="Live from space album cover"
        />
      </Card>
      <Card
        sx={{
          display: "flex",
          width: "80%",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "1%",
        }}
      >
        <CardContent sx={{ flex: "1 0 auto" }}>
          <Typography
            component="div"
            variant="h5"
            sx={{ fontFamily: "Raleway" }}
          >
            Add expenses and incomes on the fly!
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
            width={750}
            sx={{ marginTop: "3%", fontFamily: "Raleway" }}
          >
            Life has you on the go every day? You can add your expenses and
            incomes on the fly using Sparrow's fast and accesible user
            interface. You might have to wait in line at the store, but you
            won't have to worry about that on this app!
          </Typography>
        </CardContent>
        <CardMedia
          component="img"
          sx={{ width: 400, margin: "1.5% 3% 1.5% 3%" }}
          image="../resources/about-page-expenses.png"
          alt="Live from space album cover"
        />
      </Card>
      <Card
        sx={{
          display: "flex",
          width: "80%",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "1%",
        }}
      >
        <CardContent sx={{ flex: "1 0 auto" }}>
          <Typography
            component="div"
            variant="h5"
            sx={{ fontFamily: "Raleway" }}
          >
            Always keep your accounts up to date!
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
            width={750}
            sx={{ marginTop: "3%", fontFamily: "Raleway" }}
          >
            You can always see the latest balance of your accounts, as well as
            adding new ones! One account isn't up to date anymore? Then you can
            edit it, and Sparrow will take care of the rest. Did you close an
            account in real life? Then you can delete it.
          </Typography>
        </CardContent>
        <CardMedia
          component="img"
          sx={{ width: 400, margin: "1.5% 3% 1.5% 3%" }}
          image="../resources/about-page-accounts.png"
          alt="Live from space album cover"
        />
      </Card>
    </div>
  );
}
