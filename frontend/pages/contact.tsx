import "../styles/styles.scss";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import { IconButton, Snackbar, TextField } from "@mui/material";
import React, { useState } from "react";
import { useMutation } from "@apollo/client";
import { SendMessage } from "./graphql/contact.mutation.gql";
import CloseIcon from '@mui/icons-material/Close';

const emailRegex =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;

export default function Contact() {

  const [formState, setFormState] = useState({
    name: "",
    email: "",
    phoneNumber: "",
    message: "",
  });

  const [errorText, setErrorText] = useState({
    nameText: "",
    emailText: "",
    phoneNumberText: "",
    messageText: "",
  });

  const [snackBar, setSnackBar] = useState({
    open: false,
    message: "",
  });

  const [sendMessage] = useMutation(SendMessage, {
    variables: {
      name: formState.name,
      email: formState.email,
      phoneNumber: formState.phoneNumber,
      message: formState.message,
    },
  });

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (
      errorText.emailText === "" &&
      errorText.nameText === "" &&
      errorText.phoneNumberText === "" &&
      errorText.messageText === ""
    ) {
      sendMessage();
      setSnackBar({
        open: true,
        message: "Message sent successfully!",
      });
      setFormState({
        name: "",
        email: "",
        phoneNumber: "",
        message: "",
      });
    } else {
      setSnackBar({
        open: true,
        message: "The entered info is not correct!",
      });
    }
  };

  const handleClose = (event: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setSnackBar({
      open: false,
      message: "",
    });
  };

  const closeAction = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  const onNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length <= 64) {
      setErrorText({ ...errorText, nameText: "" });
      setFormState({ ...formState, name: e.target.value });
    } else {
      setErrorText({
        ...errorText,
        nameText: "Name can't have more than 64 characters!",
      });
      setFormState({ ...formState, name: e.target.value });
    }
  };

  const onEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.match(emailRegex)) {
      setErrorText({ ...errorText, emailText: "" });
      setFormState({ ...formState, email: e.target.value });
    } else {
      setErrorText({ ...errorText, emailText: "Invalid email address!" });
      setFormState({ ...formState, email: e.target.value });
    }
  };

  const onPhoneNumberChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.match(phoneRegex) || e.target.value === "") {
      setErrorText({ ...errorText, phoneNumberText: "" });
      setFormState({ ...formState, phoneNumber: e.target.value });
    } else {
      setErrorText({ ...errorText, phoneNumberText: "Invalid phone number!" });
      setFormState({ ...formState, phoneNumber: e.target.value });
    }
  };

  const onMessageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length <= 500) {
      setErrorText({ ...errorText, messageText: "" });
      setFormState({ ...formState, message: e.target.value });
    } else {
      setErrorText({
        ...errorText,
        messageText: "Message can't have more than 500 characters!",
      });
      setFormState({ ...formState, message: e.target.value });
    }
  };

  return (
    <div
      className="contact-page"
      style={{
        backgroundImage: "url('../resources/homepage-background.svg')",
        backgroundAttachment: "fixed",
        backgroundSize: "cover",
        minWidth: "100%",
      }}
    >
      <iframe
        className="gmaps-location"
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1242.6775646495805!2d25.609392079874237!3d45.63643119396867!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b35c85230fc5d5%3A0xf72864700998a4bf!2sBloc%20A13%2C%20Strada%20Ghindei%201%2C%20Bra%C8%99ov%20500288!5e0!3m2!1sro!2sro!4v1653571024800!5m2!1sro!2sro"
      />

      <div className="contact-page-content">
        <Card className="info-card" sx={{ boxShadow: 4 }}>
          <CardContent>
            <p className="title-text">All the channels are available!</p>
            <p className="content-text">We are happy if you link with us!</p>
            <a
              className="links"
              href="https://www.linkedin.com/in/robert-sperchez/"
            >
              Linkedin
            </a>
            <a className="content-text"> | </a>
            <a
              className="links"
              href="https://www.facebook.com/sperchez.robert"
            >
              Facebook
            </a>
            <a className="content-text"> | </a>
            <a className="links" href="https://twitter.com/RobertSperchez">
              Twitter
            </a>
            <p className="content-text">
              In order to get a fast answer, please send us an e-mail via our
              contact form and we will get in touch with you immediately.
            </p>
            <p className="title-text">Robert Sperchez Corp.</p>
            <p className="content-text">Address: Ghindei Street 1</p>
            <p className="content-text">
              Building A13D, Apartment 17, 4th Floor
            </p>
            <p className="content-text">500288, Brasov, Romania</p>
            <p className="content-text">Phone No.: +40 751 203 081</p>
            <a className="links" href="mailto:robert.sperchez@gmail.com">
              robert.sperchez@gmail.com
            </a>
          </CardContent>
        </Card>

        <Card className="contact-form" sx={{ boxShadow: 4 }}>
          <CardContent>
            <p className="title-text">Contact us!</p>
            <form className="form-content" onSubmit={onSubmit}>
              <TextField
                required
                value={formState.name}
                label="Full name"
                variant="filled"
                size="small"
                style={{ marginTop: "3%", width: "80%" }}
                onChange={onNameChange}
                error={errorText.nameText !== ""}
                helperText={errorText.nameText}
              />
              <TextField
                required
                value={formState.email}
                label="Email address"
                variant="filled"
                size="small"
                style={{ marginTop: "3%", width: "80%" }}
                onChange={onEmailChange}
                error={errorText.emailText !== ""}
                helperText={errorText.emailText}
              />
              <TextField
                value={formState.phoneNumber}
                label="Phone number"
                variant="filled"
                size="small"
                style={{ marginTop: "3%", width: "80%" }}
                onChange={onPhoneNumberChange}
                error={errorText.phoneNumberText !== ""}
                helperText={errorText.phoneNumberText}
              />
              <TextField
                required
                value={formState.message}
                label="Message"
                variant="filled"
                size="small"
                multiline
                style={{ marginTop: "3%", width: "80%" }}
                onChange={onMessageChange}
                error={errorText.messageText !== ""}
                helperText={errorText.messageText}
              />
              <Snackbar
                open={snackBar.open}
                autoHideDuration={6000}
                message={snackBar.message}
                onClose={handleClose}
                action={closeAction}
              />
              <Button
                type="submit"
                variant="contained"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                  marginTop: "7%",
                  backgroundColor: "#76d275",
                  width: "50%",
                  fontFamily: "Raleway",
                }}
              >
                Send message
              </Button>
            </form>
          </CardContent>
        </Card>
      </div>
    </div>
  );
}
