import "../styles/styles.scss";
import redirectImage from "../resources/login-redirect-image.svg";
import Button from "@mui/material/Button";

export default function LoginRedirect() {
  const handleLogin = () => {
    window.location.href = `http://localhost:8080/auth/login?backTo=${encodeURIComponent(
      window.location.href
    )}`;
  };

  return (
    <div
      className="login-redirect"
      style={{
        backgroundImage: "url('../resources/homepage-background.svg')",
        backgroundAttachment: "fixed",
        backgroundSize: "cover",
        minWidth: "100%",
        minHeight: "87%"
      }}
    >
      <div className="redirect-image">
        <img src={redirectImage} className="image" />
      </div>

      <div className="redirect-content">
        <h1 className="title-text">Why miss out on all the fun?</h1>
        <p className="subtitle-text">
          It seems like you haven't logged in. You can do that now!
        </p>
        <div className="redirect-button-container">
          <Button
            variant="contained"
            onClick={handleLogin}
            style={{
              backgroundColor: "#76d275",
              width: "200px",
              fontFamily: "Raleway",
            }}
          >
            Login
          </Button>
        </div>
      </div>
    </div>
  );
}
