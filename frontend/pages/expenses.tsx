import { ExpenseTable } from "@components/expenses/expense-table";
import { Card, CardContent, Typography } from "@mui/material";

export default function Expenses() {
  return (
    <div
      className="expenses-page"
      style={{
        backgroundImage: "url('../resources/homepage-background.svg')",
        backgroundAttachment: "fixed",
        backgroundSize: "cover",
        minWidth: "100%",
      }}
    >
      <Card
        className="expenses-card"
        sx={{ boxShadow: 4, backgroundColor: "#76d275" }}
      >
        <Typography variant="h4" sx={{ mt: "1%", color: "white" }}>
          Expenses
        </Typography>
        <CardContent>
          <ExpenseTable />
        </CardContent>
      </Card>
    </div>
  );
}
