import AccountsCard from "@components/dashboard/accounts-card";
import BalanceCard from "@components/dashboard/balance-card";
import BalanceHistoryCard from "@components/dashboard/balance-history-card";
import ExpensesCard from "@components/dashboard/expenses-card";
import ExpensesCategoryCard from "@components/dashboard/expenses-category-card";
import IncomesCard from "@components/dashboard/incomes-card";

export default function Dashboard() {
  return (
    <div
      className="dashboard-page"
      style={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
        minHeight: "100vh",
      }}
    >
      <div
        className="cards-container"
        style={{
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div
          className="data-cards-container"
          style={{
            width: "100%",
          }}
        >
          <AccountsCard />
          <ExpensesCard />
          <IncomesCard />
        </div>
        <div
          className="graphs-container"
          style={{
            width: "100%",
          }}
        >
          <BalanceCard />
          <BalanceHistoryCard />
          <ExpensesCategoryCard />
        </div>
      </div>
    </div>
  );
}
