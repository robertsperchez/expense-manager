import { IncomeTable } from "@components/incomes/income-table";
import { Card, CardContent, Typography } from "@mui/material";

export default function Incomes() {
  return (
    <div
      className="incomes-page"
      style={{
        backgroundImage: "url('../resources/homepage-background.svg')",
        backgroundAttachment: "fixed",
        backgroundSize: "cover",
        minWidth: "100%",
      }}
    >
      <Card
        className="incomes-card"
        sx={{ boxShadow: 4, backgroundColor: "#76d275" }}
      >
        <Typography variant="h4" sx={{ mt: "1%", color: "white" }}>
          Incomes
        </Typography>
        <CardContent>
          <IncomeTable />
        </CardContent>
      </Card>
    </div>
  );
}
