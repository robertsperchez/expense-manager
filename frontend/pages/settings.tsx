import CategoriesSection from "@components/categories/categories-section";
import SettingsSection from "@components/settings/settings-section";

export default function Settings() {
  return (
    <div
      className="settings-page"
      style={{
        backgroundImage: "url('../resources/homepage-background.svg')",
        backgroundAttachment: "fixed",
        backgroundSize: "cover",
        minWidth: "100%",
        minHeight: "120%",
      }}
    >
        <SettingsSection />
        <CategoriesSection />
    </div>
  );
}
