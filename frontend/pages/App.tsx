import { withProviders } from "@providers/providers";
import "../styles/styles.scss";
import { NavBar } from "../components/nav-bar";
import { AppFooter } from "../components/footer";
import { Router } from "@components/router/router";

function App() {

  return (
    <div className="app-container">
      <NavBar />
      <Router />
      <AppFooter />
    </div>
  );
}

export default withProviders(App);
