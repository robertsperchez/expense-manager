import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import config from '../config';
import { createHttpWsLink } from './apollo-http-ws';
import { WebSocketLink } from './apollo-ws';

const httpLink = new HttpLink({
	uri: config.api.graphqlUrl,
	credentials: 'include',
});

console.log(config.api.graphqlUrl)

const wsLink = new WebSocketLink({
	url: config.api.subscriptionsUrl,
});

const httpWslink = createHttpWsLink(httpLink, wsLink);

export const apollo = new ApolloClient({
	cache: new InMemoryCache(),
	uri: config.api.graphqlUrl,
	credentials: "include"
});
