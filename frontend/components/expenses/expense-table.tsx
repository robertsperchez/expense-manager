import { useMutation, useQuery } from "@apollo/client";
import {
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  Icon,
  Checkbox,
  TableSortLabel,
  Toolbar,
  IconButton,
  Grid,
  Box,
} from "@mui/material";
import { useUser } from "@providers/user";
import { formatStringDate } from "@utils/methods";
import { useState } from "react";
import { CountExpenses, GetExpenses } from "./expenses.query.gql";
import DeleteIcon from "@mui/icons-material/Delete";
import { DeleteExpense } from "./expense.mutation.gql";
import ExpenseForm from "./expense-form";
import ExpenseEditForm from "./expense-edit-form";

interface Column {
  id: string;
  label: string;
  minWidth?: number;
  sortable: boolean;
  align?: "right";
  format?: (value: number) => string;
}

type Order = "asc" | "desc";

export function ExpenseTable() {
  const [order, setOrder] = useState<Order>("desc");
  const [sortField, setSortField] = useState<string>("expenseDate");
  const [numSelected, setNumSelected] = useState<number>(0);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [selected, setSelected] = useState<readonly string[]>([]);
  const user = useUser();

  const { data, loading } = useQuery(GetExpenses, {
    variables: {
      userId: user.id,
      page: page,
      size: rowsPerPage,
      field: sortField,
      sort: order,
    },
  });

  const { data: countData, loading: countLoading } = useQuery(CountExpenses, {
    variables: {
      userId: user.id,
    },
  });

  const isSelected = (id: string) => selected.indexOf(id) !== -1;

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = data!.expenseList!.map((e) => e.id);
      setSelected(newSelecteds);
      setNumSelected(newSelecteds.length);
      return;
    }
    setSelected([]);
    setNumSelected(0);
  };

  const handleItemClick = (event: React.MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
    setNumSelected(newSelected.length);
  };

  const createSortHandler =
    (field: string) => (event: React.MouseEvent<unknown>) => {
      setSortField(field);
      if (order === "asc") {
        setOrder("desc");
      } else {
        setOrder("asc");
      }
    };

  const [deleteExpense] = useMutation(DeleteExpense);

  const handleDelete = () => {
    selected.forEach((expenseId) => {
      deleteExpense({
        variables: {
          id: expenseId,
        },
        refetchQueries: [GetExpenses, CountExpenses, "GetAccounts", "GetMe", "GetExpensesSumByCategory"],
      });
    });
    setSelected([]);
    setNumSelected(0);
  };

  const columns: readonly Column[] = [
    { id: "title", label: "Title", minWidth: 170, sortable: true },
    { id: "category", label: "Category", minWidth: 170, sortable: true },
    { id: "amount", label: "Amount", minWidth: 170, sortable: true },
    { id: "expenseDate", label: "Expense Date", minWidth: 170, sortable: true },
    { id: "account", label: "Account", minWidth: 170, sortable: true },
    { id: "comment", label: "Comments", minWidth: 170, sortable: true },
    { id: "actions", label: "Actions", minWidth: 70, sortable: false },
  ];

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell padding="checkbox">
                <Checkbox
                  color="primary"
                  indeterminate={
                    loading == false
                      ? numSelected > 0 &&
                        numSelected < data!.expenseList!.length
                      : false
                  }
                  checked={
                    loading == false
                      ? data!.expenseList!.length > 0 &&
                        numSelected === data!.expenseList!.length
                      : false
                  }
                  onChange={handleSelectAllClick}
                />
              </TableCell>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  <TableSortLabel
                    disabled={!column.sortable}
                    active={sortField === column.id}
                    direction={order}
                    onClick={createSortHandler(column.id)}
                  >
                    {column.label}
                  </TableSortLabel>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data?.expenseList!.map((expense) => {
              const isItemSelected = isSelected(expense.id);
              return (
                <TableRow
                  hover
                  onClick={(event) => handleItemClick(event, expense.id)}
                  role="checkbox"
                  aria-checked={isItemSelected}
                  tabIndex={-1}
                  key={expense.id}
                  selected={isItemSelected}
                >
                  <TableCell padding="checkbox">
                    <Checkbox color="primary" checked={isItemSelected} />
                  </TableCell>
                  <TableCell>{expense.title}</TableCell>
                  <TableCell sx={{ display: "flex" }}>
                    <Icon sx={{ color: expense.category.color, mr: "3%" }}>
                      {expense.category.iconName}
                    </Icon>
                    {expense.category.name}
                  </TableCell>
                  <TableCell sx={{ color: "red" }}>
                    {expense.amount} {user.settings.currencyType}
                  </TableCell>
                  <TableCell>{formatStringDate(expense.expenseDate)}</TableCell>
                  <TableCell>{expense.account.name}</TableCell>
                  <TableCell>
                    {(expense.comment == null || expense.comment === "") ? "-" : expense.comment}
                  </TableCell>
                  <TableCell>
                    <ExpenseEditForm expenseId={expense.id} />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={countLoading == false ? countData!.expensesNumber! : -1}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />

      <Toolbar
        className="expense-table-toolbar"
        style={{
          backgroundColor: "#e2e2e2",
        }}
      >
        <Grid container spacing={3}>
          <Grid
            item
            xs={3}
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <ExpenseForm />
          </Grid>
          <Grid item xs>
            <Box sx={{ flexGrow: 1 }} />
          </Grid>
          <Grid
            item
            xs={3}
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-end",
            }}
          >
            <p>{numSelected} items selected.</p>
            <IconButton onClick={handleDelete}>
              <DeleteIcon />
            </IconButton>
          </Grid>
        </Grid>
      </Toolbar>
    </Paper>
  );
}
