import AppBar from "@mui/material/AppBar";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Toolbar from "@mui/material/Toolbar";
import "../styles/styles.scss";
import sparrowLogoGray from "../resources/sparrow-logo-gray.png";
import Box from "@mui/material/Box";
import { createRoute } from "../routes/routes";
import { Link, useNavigate } from "react-router-dom";
import { useAuth } from "@providers/auth";
import IconButton from "@mui/material/IconButton";
import Avatar from "@mui/material/Avatar";
import { useState } from "react";
import { ListItemIcon, Menu, MenuItem } from "@mui/material";
import {
  Settings,
  Logout,
  Dashboard,
  MoneyOff,
  AttachMoney,
  CreditCard,
} from "@mui/icons-material";

export function NavBar() {
  const auth = useAuth();
  const navigate = useNavigate();
  const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);
  const [anchorElMenu, setAnchorElMenu] = useState<null | HTMLElement>(null);
  const openUserMenu = Boolean(anchorElUser);
  const openMenu = Boolean(anchorElMenu);

  const handleAvatarClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleMenuClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElMenu(event.currentTarget);
  };

  const handleUserMenuClose = () => {
    setAnchorElUser(null);
  };

  const handleMenuClose = () => {
    setAnchorElMenu(null);
  };

  const handleLogin = () => {
    window.location.href = `http://localhost:8080/auth/login?backTo=${encodeURIComponent(
      window.location.href
    )}`;
  };

  const handleLogoClick = () => {
    navigate(createRoute.home());
  };

  const handleDashboardRedirect = () => {
    navigate(createRoute.dashboard());
    handleMenuClose();
  };

  const handleExpensesRedirect = () => {
    navigate(createRoute.expenses());
    handleMenuClose();
  };

  const handleIncomesRedirect = () => {
    navigate(createRoute.incomes());
    handleMenuClose();
  };

  const handleAccountsRedirect = () => {
    navigate(createRoute.accounts());
    handleMenuClose();
  };

  const handleSettingsRedirect = () => {
    navigate(createRoute.settings());
    handleMenuClose();
  };

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <img
            src={sparrowLogoGray}
            alt="Sparrow"
            className="header-logo"
            onClick={() => handleLogoClick()}
          />
          <span className="header-logo-text" onClick={() => handleLogoClick()}>
            sparrow
          </span>
          <Box sx={{ flexGrow: 1 }} />
          {auth.loggedIn && (
            <div>
              <Button
                variant="text"
                color="inherit"
                style={{
                  width: "100px",
                }}
                aria-controls={openMenu ? "pages-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={openMenu ? "true" : undefined}
                onClick={handleMenuClick}
              >
                Menu
              </Button>
              <Menu
                id="pages-menu"
                anchorEl={anchorElMenu}
                open={openMenu}
                onClose={handleMenuClose}
                PaperProps={{
                  elevation: 0,
                  sx: {
                    overflow: "visible",
                    filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                    mt: 1.5,
                    "& .MuiAvatar-root": {
                      width: 32,
                      height: 32,
                      ml: -0.5,
                      mr: 1,
                    },
                    "&:before": {
                      content: '""',
                      display: "block",
                      position: "absolute",
                      top: 0,
                      right: 14,
                      width: 10,
                      height: 10,
                      bgcolor: "background.paper",
                      transform: "translateY(-50%) rotate(45deg)",
                      zIndex: 0,
                    },
                  },
                }}
                transformOrigin={{ horizontal: "right", vertical: "top" }}
                anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
              >
                <MenuItem onClick={handleDashboardRedirect}>
                  <ListItemIcon>
                    <Dashboard fontSize="small" />
                  </ListItemIcon>
                  Dashboard
                </MenuItem>
                <MenuItem onClick={handleExpensesRedirect}>
                  <ListItemIcon>
                    <MoneyOff fontSize="small" />
                  </ListItemIcon>
                  Expenses
                </MenuItem>
                <MenuItem onClick={handleIncomesRedirect}>
                  <ListItemIcon>
                    <AttachMoney fontSize="small" />
                  </ListItemIcon>
                  Incomes
                </MenuItem>
                <MenuItem onClick={handleAccountsRedirect}>
                  <ListItemIcon>
                    <CreditCard fontSize="small" />
                  </ListItemIcon>
                  Accounts
                </MenuItem>
              </Menu>
            </div>
          )}

          <Button
            variant="text"
            color="inherit"
            style={{
              width: "100px",
            }}
            component={Link}
            to={createRoute.about()}
          >
            About
          </Button>
          <Button
            variant="text"
            component={Link}
            color="inherit"
            to={createRoute.contact()}
            style={{
              width: "100px",
              marginRight: "3%",
            }}
          >
            Contact
          </Button>

          {/* In case user is not logged in. */}
          {!auth.loggedIn && !auth.loading && (
            <Button
              variant="contained"
              onClick={handleLogin}
              style={{
                backgroundColor: "#282828",
                width: "130px",
              }}
            >
              Login
            </Button>
          )}

          {/* In case user is logged in. */}
          {auth.loggedIn && (
            <div>
              <span className="header-welcome-text">
                Welcome, {auth.user?.givenName}!
              </span>
              <IconButton
                onClick={handleAvatarClick}
                size="small"
                sx={{ ml: 2 }}
                aria-controls={openUserMenu ? "account-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={openUserMenu ? "true" : undefined}
              >
                <Avatar
                  sx={{ width: 32, height: 32 }}
                  src={auth.user?.profilePicture!}
                />
              </IconButton>
              <Menu
                anchorEl={anchorElUser}
                id="account-menu"
                open={openUserMenu}
                onClose={handleUserMenuClose}
                onClick={handleUserMenuClose}
                PaperProps={{
                  elevation: 0,
                  sx: {
                    overflow: "visible",
                    filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                    mt: 1.5,
                    "& .MuiAvatar-root": {
                      width: 32,
                      height: 32,
                      ml: -0.5,
                      mr: 1,
                    },
                    "&:before": {
                      content: '""',
                      display: "block",
                      position: "absolute",
                      top: 0,
                      right: 14,
                      width: 10,
                      height: 10,
                      bgcolor: "background.paper",
                      transform: "translateY(-50%) rotate(45deg)",
                      zIndex: 0,
                    },
                  },
                }}
                transformOrigin={{ horizontal: "right", vertical: "top" }}
                anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
              >
                <MenuItem onClick={handleSettingsRedirect}>
                  <ListItemIcon>
                    <Settings fontSize="small" />
                  </ListItemIcon>
                  Settings
                </MenuItem>
                <MenuItem onClick={auth.logout}>
                  <ListItemIcon>
                    <Logout fontSize="small" />
                  </ListItemIcon>
                  Logout
                </MenuItem>
              </Menu>
            </div>
          )}
        </Toolbar>
      </Container>
    </AppBar>
  );
}
