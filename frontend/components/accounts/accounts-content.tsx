import { useMutation, useQuery } from "@apollo/client";
import {
  Card,
  CardContent,
  CircularProgress,
  Grid,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";
import { useUser } from "@providers/user";
import { GetAccounts } from "./accounts.query.gql";
import DeleteIcon from "@mui/icons-material/Delete";
import { DeleteAccount } from "./accounts.mutation.gql";
import AccountForm from "./account-form";
import EditAccountForm from "./account-edit-form";

export default function AccountsContent() {
  const user = useUser();
  const [deleteAccount] = useMutation(DeleteAccount);

  const { data, loading } = useQuery(GetAccounts, {
    variables: {
      userId: user.id,
    },
    nextFetchPolicy: "cache-and-network",
  });

  const handleDelete = (accountId: string) => {
    deleteAccount({
      variables: {
        id: accountId,
      },
      refetchQueries: [GetAccounts, "GetAccountsPaginated", "GetMe", "GetExpensesSumByCategory"],
    });
  };

  return (
    <CardContent sx={{ width: "90%" }}>
      <Grid
        container
        spacing={{ xs: 2, md: 5 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
      >
        {loading == false ? (
          data?.accountList!.map((account) => {
            return (
              <Grid item xs={2} sm={4} md={4} key={account.id}>
                <Card
                  className="accounts-info-card"
                  sx={{
                    boxShadow: 4,
                    backgroundImage: "url('../resources/card-background.svg')",
                    backgroundAttachment: "fixed",
                    backgroundSize: "cover",
                    minWidth: "100%",
                  }}
                >
                  <CardContent>
                    <Typography variant="h6" sx={{ mt: "1%" }}>
                      {account.name}
                    </Typography>
                    <a>Amount: </a>
                    {account.amount < 0 ? (
                      <a style={{ color: "red", fontWeight: "bold" }}>
                        {account.amount.toFixed(2)} {user.settings.currencyType}
                      </a>
                    ) : (
                      <a style={{ color: "#43a047", fontWeight: "bold" }}>
                        {account.amount.toFixed(2)} {user.settings.currencyType}
                      </a>
                    )}
                  </CardContent>
                  <div style={{display: "flex"}}>
                    <IconButton onClick={() => handleDelete(account.id)}>
                      <DeleteIcon color="warning" />
                    </IconButton>
                    <EditAccountForm accountId={account.id} />
                  </div>
                </Card>
              </Grid>
            );
          })
        ) : (
          <CircularProgress />
        )}
      </Grid>
      <Toolbar
        className="expense-table-toolbar"
        style={{
          backgroundColor: "#e2e2e2",
          marginTop: "2%",
          borderRadius: "5px",
          boxShadow: "0px 3px 8px rgba(0, 0, 0, 0.24)",
        }}
      >
        <AccountForm />
      </Toolbar>
    </CardContent>
  );
}
