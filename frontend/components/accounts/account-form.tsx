import { Add } from "@mui/icons-material";
import {
  Button,
  Modal,
  Box,
  Typography,
  TextField,
  Snackbar,
  IconButton,
} from "@mui/material";
import { useUser } from "@providers/user";
import React from "react";
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { useMutation } from "@apollo/client";
import { AddAccount } from "./accounts.mutation.gql";

const modalStyle = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  textAlign: "center",
};

interface AccountInput {
  name: string;
  amount: string;
}

const sumRegex = /^[+-]?([0-9]+\.?[0-9]*|\.[0-9]+)$/;

export default function AccountForm() {
  const [openModal, setOpenModal] = useState(false);
  const user = useUser();
  const [formState, setFormState] = useState<AccountInput>({
    name: "",
    amount: "",
  });

  const [errorText, setErrorText] = useState({
    nameText: "",
    amountText: "",
  });

  const [snackBar, setSnackBar] = useState({
    open: false,
    message: "",
  });

  const handleModalOpen = () => setOpenModal(true);
  const handleModalClose = () => setOpenModal(false);

  const onNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length <= 64) {
      setErrorText({ ...errorText, nameText: "" });
      setFormState({ ...formState, name: e.target.value });
    } else {
      setErrorText({
        ...errorText,
        nameText: "The name can't have more than 64 characters!",
      });
      setFormState({ ...formState, name: e.target.value });
    }
  };

  const onAmountChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.match(sumRegex) || e.target.value === "") {
      setErrorText({ ...errorText, amountText: "" });
      setFormState({ ...formState, amount: e.target.value });
    } else {
      setErrorText({ ...errorText, amountText: "Invalid sum!" });
      setFormState({ ...formState, amount: e.target.value });
    }
  };

  const [addAccount] = useMutation(AddAccount, {
    variables: {
      account: {
        userId: user.id,
        amount: Number(formState.amount),
        name: formState.name,
      },
    },
    refetchQueries: ["GetAccounts", "GetAccountsPaginated", "GetMe"],
  });

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (errorText.nameText === "" && errorText.amountText === "") {
      addAccount();
      setSnackBar({
        open: true,
        message: "Account added successfully!",
      });
      setFormState({
        name: "",
        amount: "",
      });
      setOpenModal(false);
    } else {
      setSnackBar({
        open: true,
        message: "The entered info is not correct!",
      });
    }
  };

  const handleSnackbarClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setSnackBar({
      open: false,
      message: "",
    });
  };

  const closeAction = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleSnackbarClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  return (
    <div className="account-form">
      <Button
        variant="contained"
        style={{
          backgroundColor: "#76d275",
          width: "200px",
        }}
        onClick={handleModalOpen}
      >
        <Add fontSize="small" sx={{ mr: "2%" }} />
        Add Account
      </Button>
      <Modal
        open={openModal}
        onClose={handleModalClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <Typography variant="h5">Add Account</Typography>
          <form className="form-content" onSubmit={onSubmit}>
            <TextField
              required
              value={formState.name}
              label="Name"
              variant="filled"
              size="small"
              style={{ marginTop: "3%", width: "80%" }}
              onChange={onNameChange}
              error={errorText.nameText !== ""}
              helperText={errorText.nameText}
            />
            <TextField
              required
              value={formState.amount}
              label="Amount"
              variant="filled"
              size="small"
              style={{ marginTop: "3%", width: "80%" }}
              onChange={onAmountChange}
              error={errorText.amountText !== ""}
              helperText={errorText.amountText}
            />
            <Button
              type="submit"
              variant="contained"
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: "7%",
                backgroundColor: "#76d275",
                width: "70%",
                fontFamily: "Raleway",
              }}
            >
              Create Account
            </Button>
          </form>
        </Box>
      </Modal>
      <Snackbar
        open={snackBar.open}
        autoHideDuration={6000}
        message={snackBar.message}
        onClose={handleSnackbarClose}
        action={closeAction}
      />
    </div>
  );
}
