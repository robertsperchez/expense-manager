import { AppRoute } from "../../routes";
import { WithAuth } from "./with-auth";

interface Props {
  route: AppRoute;
}

export function RouteHandler(props: Props) {
  if (props.route.requireAuth === false) {
    return <props.route.component />;
  }

  return (
    <WithAuth>
      <props.route.component />
    </WithAuth>
  );
}
