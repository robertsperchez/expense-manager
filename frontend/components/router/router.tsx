import { ReactNode, Suspense } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { routes } from '../../routes';
import { RouteHandler } from './route-handler';

export { Link as RouterLink } from 'react-router-dom';

interface LazyRouteProps {
	element: ReactNode;
	fallback?: NonNullable<ReactNode>;
}

function LazyRoute(props: LazyRouteProps) {
	return <Suspense fallback={props.fallback ?? <></>}>{props.element}</Suspense>;
}

export function Router() {
	return (
		<Routes>
			{routes.map((route) => (
				<Route
					key={route.path}
					path={route.path}
					element={<LazyRoute fallback={route.fallback} element={<RouteHandler route={route} />} />}
				/>
			))}

			<Route index={true} element={<Navigate to="/home" />} />
		</Routes>
	);
}