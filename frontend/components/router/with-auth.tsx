import { CircularProgress } from "@mui/material";
import { useAuth } from "@providers/auth";
import { UserProvider } from "@providers/user";
import { ReactNode } from "react";
import LoginRedirect from "../../pages/login-redirect";

interface Props {
  children: ReactNode;
}

export function WithAuth(props: Props) {
  const auth = useAuth();

  if (auth.error) {
    return <LoginRedirect />;
  }

  if (auth.loading || !auth.user) {
    return <CircularProgress />;
  }

  return <UserProvider value={auth.user}>{props.children}</UserProvider>;
}
