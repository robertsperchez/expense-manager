import {
  Button,
  Card,
  CardContent,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Snackbar,
  Typography,
} from "@mui/material";
import { useUser } from "@providers/user";
import React from "react";
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { UpdateSettings } from "./settings.mutation.gql";
import { useMutation } from "@apollo/client";
import {
  GetExpenses,
  CountExpenses,
  GetExpense,
} from "@components/expenses/expenses.query.gql";

interface Currency {
  value: string;
  label: string;
}

export default function SettingsSection() {
  const currencies: readonly Currency[] = [
    { value: "EUR", label: "Euro" },
    { value: "USD", label: "US Dollar" },
    { value: "RON", label: "Romanian Leu" },
    { value: "JPY", label: "Japanese Yen" },
    { value: "GBP", label: "British Pound" },
    { value: "CAD", label: "Canadian Dollar" },
    { value: "AUD", label: "Australian Dollar" },
    { value: "CHF", label: "Swiss Franc" },
    { value: "SEK", label: "Swedish Krona" },
    { value: "NOK", label: "Norwegian Krone" },
    { value: "DKK", label: "Danish Krone" },
    { value: "PLN", label: "Polish Zloty" },
    { value: "BRL", label: "Brazilian Real" },
    { value: "CZK", label: "Czech Koruna" },
    { value: "HUF", label: "Hungarian Forint" },
    { value: "ILS", label: "Israeli Shekel" },
    { value: "MXN", label: "Mexican Peso" },
  ];
  const user = useUser();
  const [currency, setCurrency] = useState(user.settings.currencyType);

  const onCurrencyChange = (e: SelectChangeEvent) => {
    setCurrency(e.target.value);
  };

  const [snackBar, setSnackBar] = useState({
    open: false,
    message: "",
  });

  const handleSnackbarClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setSnackBar({
      open: false,
      message: "",
    });
  };

  const closeAction = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleSnackbarClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  const [updateCurrency] = useMutation(UpdateSettings, {
    variables: {
      settings: {
        userId: user.id,
        currencyType: currency,
      },
    },
    refetchQueries: [GetExpenses, CountExpenses, GetExpense, "GetAccounts", "GetMe", "GetExpensesSumByCategory"],
  });

  const onCurrencySubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    updateCurrency();
    setSnackBar({
      open: true,
      message: "Currency updated successfully!",
    });
  };

  return (
    <Card className="settings-card" sx={{ boxShadow: 4 }}>
      <CardContent>
        <Typography fontWeight="bold" variant="h4">Settings</Typography>
        <Divider sx={{mt:"1.5%", mb:"1.5%"}}/>
        <form className="form-content" onSubmit={onCurrencySubmit}>
          <Grid
            container
            spacing={3}
            height="10%"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Grid item xs={2.1}>
              <a style={{fontSize:"110%"}}>Change the currency type: </a>
            </Grid>
            <Grid item xs>
              <FormControl variant="filled" sx={{ textAlign: "left" }}>
                <InputLabel>Currency</InputLabel>
                <Select sx={{width: "300px", height: "50px"}} value={currency} onChange={onCurrencyChange}>
                  {currencies.map((ccy) => {
                    return (
                      <MenuItem key={ccy.value} value={ccy.value}>
                        {ccy.label}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={3}>
              <Button
                type="submit"
                variant="contained"
                style={{
                  marginLeft: "auto",
                  marginRight: "auto",
                  backgroundColor: "#76d275",
                  width: "60%",
                }}
              >
                Change currency
              </Button>
            </Grid>
          </Grid>
        </form>
      </CardContent>
      <Snackbar
        open={snackBar.open}
        autoHideDuration={6000}
        message={snackBar.message}
        onClose={handleSnackbarClose}
        action={closeAction}
      />
    </Card>
  );
}
