import { Add } from "@mui/icons-material";
import {
  Button,
  Modal,
  Box,
  Typography,
  TextField,
  Snackbar,
  IconButton,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Icon,
  SelectChangeEvent,
} from "@mui/material";
import { useUser } from "@providers/user";
import React from "react";
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { useMutation } from "@apollo/client";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { AddIncome } from "./incomes.mutation.gql";
import { CountIncomes, GetIncomes } from "./incomes.query.gql";

const modalStyle = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  textAlign: "center",
};

interface IncomeInput {
  title: string;
  amount: string;
  incomeDate: Date | null;
  comment?: string;
  accountId: string;
  categoryId: string;
}

const sumRegex = /^(\d+\.)?\d+$/;

export default function IncomeForm() {
  const [openModal, setOpenModal] = useState(false);
  const user = useUser();
  const [formState, setFormState] = useState<IncomeInput>({
    title: "",
    amount: "",
    incomeDate: null,
    comment: "",
    accountId: "",
    categoryId: "",
  });

  const [errorText, setErrorText] = useState({
    titleText: "",
    commentText: "",
    amountText: "",
    dateText: "",
  });

  const [snackBar, setSnackBar] = useState({
    open: false,
    message: "",
  });

  const handleModalOpen = () => setOpenModal(true);
  const handleModalClose = () => setOpenModal(false);

  const onTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length <= 128) {
      setErrorText({ ...errorText, titleText: "" });
      setFormState({ ...formState, title: e.target.value });
    } else {
      setErrorText({
        ...errorText,
        titleText: "The title can't have more than 128 characters!",
      });
      setFormState({ ...formState, title: e.target.value });
    }
  };

  const onCommentChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length <= 512) {
      setErrorText({ ...errorText, commentText: "" });
      setFormState({ ...formState, comment: e.target.value });
    } else {
      setErrorText({
        ...errorText,
        commentText: "The comment can't have more than 512 characters!",
      });
      setFormState({ ...formState, comment: e.target.value });
    }
  };

  const onAmountChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.match(sumRegex) || e.target.value === "") {
      setErrorText({ ...errorText, amountText: "" });
      setFormState({ ...formState, amount: e.target.value });
    } else {
      setErrorText({ ...errorText, amountText: "Invalid sum!" });
      setFormState({ ...formState, amount: e.target.value });
    }
  };

  const onDateChange = (newValue: Date | null) => {
    if (newValue !== null) {
      setErrorText({ ...errorText, dateText: "" });
      setFormState({ ...formState, incomeDate: newValue });
    } else {
      setErrorText({ ...errorText, dateText: "Invalid date!" });
      setFormState({ ...formState, incomeDate: newValue });
    }
  };

  const onCategoryChange = (e: SelectChangeEvent) => {
    setFormState({ ...formState, categoryId: e.target.value });
  };

  const onAccountChange = (e: SelectChangeEvent) => {
    setFormState({ ...formState, accountId: e.target.value });
  };

  const [addIncome] = useMutation(AddIncome, {
    variables: {
      income: {
        userId: user.id,
        accountId: formState.accountId,
        categoryId: formState.categoryId,
        amount: Number(formState.amount),
        incomeDate: formState.incomeDate,
        title: formState.title,
        comment: formState.comment,
      },
    },
    refetchQueries: ["GetIncomes", CountIncomes, "GetAccounts", "GetMe"],
  });

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (
      errorText.titleText === "" &&
      errorText.amountText === "" &&
      errorText.commentText === "" &&
      errorText.dateText === ""
    ) {
      addIncome();
      setSnackBar({
        open: true,
        message: "Income added successfully!",
      });
      setFormState({
        title: "",
        amount: "",
        incomeDate: null,
        comment: "",
        accountId: "",
        categoryId: "",
      });
      setOpenModal(false);
    } else {
      setSnackBar({
        open: true,
        message: "The entered info is not correct!",
      });
    }
  };

  const handleSnackbarClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setSnackBar({
      open: false,
      message: "",
    });
  };

  const closeAction = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleSnackbarClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  return (
    <div className="income-form">
      <Button
        variant="contained"
        style={{
          backgroundColor: "#76d275",
          width: "150px",
        }}
        onClick={handleModalOpen}
      >
        <Add fontSize="small" sx={{ mr: "2%" }} />
        Add Income
      </Button>
      <Modal
        open={openModal}
        onClose={handleModalClose}
      >
        <Box sx={modalStyle}>
          <Typography variant="h5">Add Income</Typography>
          <form className="form-content" onSubmit={onSubmit}>
            <TextField
              required
              value={formState.title}
              label="Title"
              variant="filled"
              size="small"
              style={{ marginTop: "3%", width: "80%" }}
              onChange={onTitleChange}
              error={errorText.titleText !== ""}
              helperText={errorText.titleText}
            />
            <TextField
              required
              value={formState.amount}
              label="Amount"
              variant="filled"
              size="small"
              style={{ marginTop: "3%", width: "80%" }}
              onChange={onAmountChange}
              error={errorText.amountText !== ""}
              helperText={errorText.amountText}
            />
            <DesktopDatePicker
              label="Income Date"
              inputFormat="dd/MM/yyyy"
              value={formState.incomeDate}
              onChange={onDateChange}
              renderInput={(params) => (
                <TextField
                  required
                  variant="filled"
                  style={{ marginTop: "3%", width: "80%" }}
                  error={errorText.dateText !== ""}
                  helperText={errorText.dateText}
                  {...params}
                />
              )}
            />
            <TextField
              value={formState.comment}
              label="Comment"
              variant="filled"
              size="small"
              style={{ marginTop: "3%", width: "80%" }}
              onChange={onCommentChange}
              error={errorText.commentText !== ""}
              helperText={errorText.commentText}
            />
            <FormControl
              variant="filled"
              sx={{ marginTop: "3%", width: "80%", textAlign: "left" }}
            >
              <InputLabel>Category</InputLabel>
              <Select
                required
                value={formState.categoryId}
                onChange={onCategoryChange}
              >
                {user.categories!.map((category) => {
                  return (
                    <MenuItem key={category.id} value={category.id}>
                      <Icon sx={{ color: category.color, mr: "3%" }}>
                        {category.iconName}
                      </Icon>
                      {category.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
            <FormControl
              variant="filled"
              sx={{ marginTop: "3%", width: "80%", textAlign: "left" }}
            >
              <InputLabel>Account</InputLabel>
              <Select
                required
                value={formState.accountId}
                onChange={onAccountChange}
              >
                {user.accounts!.map((account) => {
                  return (
                    <MenuItem key={account.id} value={account.id}>
                      {account.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
            <Button
              type="submit"
              variant="contained"
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: "7%",
                backgroundColor: "#76d275",
                width: "70%",
                fontFamily: "Raleway",
              }}
            >
              Create Income
            </Button>
          </form>
        </Box>
      </Modal>
      <Snackbar
        open={snackBar.open}
        autoHideDuration={6000}
        message={snackBar.message}
        onClose={handleSnackbarClose}
        action={closeAction}
      />
    </div>
  );
}
