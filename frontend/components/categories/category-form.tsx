import { Add } from "@mui/icons-material";
import {
  Button,
  Modal,
  Box,
  Typography,
  TextField,
  Snackbar,
  IconButton,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Icon,
  SelectChangeEvent,
} from "@mui/material";
import { useUser } from "@providers/user";
import React from "react";
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { useMutation } from "@apollo/client";
import { AddCategory } from "./category.mutation.gql";
import { CountCategories, GetCategories } from "./category.query.gql";
import { getIconList } from "@utils/constants";
import { HexColorPicker } from "react-colorful";
const modalStyle = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  textAlign: "center",
};

interface CategoryInput {
  iconName: string;
  name: string;
  color: string;
}

export default function CategoryForm() {
  const [openModal, setOpenModal] = useState(false);
  const user = useUser();
  const [formState, setFormState] = useState<CategoryInput>({
    iconName: "",
    name: "",
    color: "#000000",
  });

  const [errorText, setErrorText] = useState({
    nameText: "",
  });

  const [snackBar, setSnackBar] = useState({
    open: false,
    message: "",
  });

  const handleModalOpen = () => setOpenModal(true);
  const handleModalClose = () => setOpenModal(false);

  const onNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length >= 2 && e.target.value.length <= 64) {
      setErrorText({ ...errorText, nameText: "" });
      setFormState({ ...formState, name: e.target.value });
    } else {
      setErrorText({
        ...errorText,
        nameText: "The name must be between 2 and 64 characters!",
      });
      setFormState({ ...formState, name: e.target.value });
    }
  };

  const onIconChange = (e: SelectChangeEvent) => {
    setFormState({ ...formState, iconName: e.target.value });
  };

  const onColorChange = (newColor: string) => {
    setFormState({ ...formState, color: newColor });
  };

  const [addCategory] = useMutation(AddCategory, {
    variables: {
      category: {
        userId: user.id,
        iconName: formState.iconName,
        name: formState.name,
        color: formState.color,
      },
    },
    refetchQueries: [GetCategories, CountCategories, "GetMe"],
  });

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (errorText.nameText === "") {
      addCategory();
      setSnackBar({
        open: true,
        message: "Category added successfully!",
      });
      setFormState({
        iconName: "",
        name: "",
        color: "",
      });
      setOpenModal(false);
    } else {
      setSnackBar({
        open: true,
        message: "The entered info is not correct!",
      });
    }
  };

  const handleSnackbarClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setSnackBar({
      open: false,
      message: "",
    });
  };

  const closeAction = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleSnackbarClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  return (
    <div className="category-form">
      <Button
        variant="contained"
        style={{
          backgroundColor: "#76d275",
          width: "180px",
        }}
        onClick={handleModalOpen}
      >
        <Add fontSize="small" sx={{ mr: "2%" }} />
        Add Category
      </Button>
      <Modal open={openModal} onClose={handleModalClose}>
        <Box sx={modalStyle}>
          <Typography variant="h5">Add Category</Typography>
          <form className="form-content" onSubmit={onSubmit}>
            <TextField
              required
              value={formState.name}
              label="Title"
              variant="filled"
              size="small"
              style={{ marginTop: "3%", width: "80%" }}
              onChange={onNameChange}
              error={errorText.nameText !== ""}
              helperText={errorText.nameText}
            />
            <FormControl
              variant="filled"
              sx={{ marginTop: "3%", width: "80%", textAlign: "left" }}
            >
              <InputLabel required>Icon</InputLabel>
              <Select
                required
                value={formState.iconName}
                onChange={onIconChange}
              >
                {getIconList().map((icon) => {
                  return (
                    <MenuItem key={icon.value} value={icon.value}>
                      <Icon sx={{ mr: "3%" }}>{icon.value}</Icon>
                      {icon.label}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
            <FormControl
              variant="filled"
              sx={{ marginTop: "3%", width: "80%", textAlign: "left", ml: "auto", mr: "auto" }}
            >
              <HexColorPicker style={{width: "100%"}} color={formState.color} onChange={onColorChange} />
            </FormControl>
            <Button
              type="submit"
              variant="contained"
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                marginTop: "7%",
                backgroundColor: "#76d275",
                width: "70%",
                fontFamily: "Raleway",
              }}
            >
              Create Category
            </Button>
          </form>
        </Box>
      </Modal>
      <Snackbar
        open={snackBar.open}
        autoHideDuration={6000}
        message={snackBar.message}
        onClose={handleSnackbarClose}
        action={closeAction}
      />
    </div>
  );
}
