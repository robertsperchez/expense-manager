import { useMutation, useQuery } from "@apollo/client";
import {
  CountExpenses,
  GetExpenses,
} from "@components/expenses/expenses.query.gql";
import {
  CountIncomes,
  GetIncomes,
} from "@components/incomes/incomes.query.gql";
import {
  Card,
  CardContent,
  Typography,
  Divider,
  Paper,
  Box,
  Checkbox,
  Grid,
  Icon,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
  Toolbar,
} from "@mui/material";
import { useUser } from "@providers/user";
import DeleteIcon from "@mui/icons-material/Delete";
import { useState } from "react";
import { DeleteCategory } from "./category.mutation.gql";
import { CountCategories, GetCategories } from "./category.query.gql";
import CategoryForm from "./category-form";
import CategoryEditForm from "./category-edit-form";

interface Column {
  id: string;
  label: string;
  minWidth?: number;
  sortable: boolean;
  align?: "right";
  format?: (value: number) => string;
}

type Order = "asc" | "desc";

export default function CategoriesSection() {
  const [order, setOrder] = useState<Order>("asc");
  const [sortField, setSortField] = useState<string>("name");
  const [numSelected, setNumSelected] = useState<number>(0);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [selected, setSelected] = useState<readonly string[]>([]);
  const user = useUser();

  const { data, loading } = useQuery(GetCategories, {
    variables: {
      userId: user.id,
      page: page,
      size: rowsPerPage,
      field: sortField,
      sort: order,
    },
  });

  const { data: countData, loading: countLoading } = useQuery(CountCategories, {
    variables: {
      userId: user.id,
    },
  });

  const isSelected = (id: string) => selected.indexOf(id) !== -1;

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = data!.categoryList!.map((c) => c.id);
      setSelected(newSelecteds);
      setNumSelected(newSelecteds.length);
      return;
    }
    setSelected([]);
    setNumSelected(0);
  };

  const handleItemClick = (event: React.MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
    setNumSelected(newSelected.length);
  };

  const createSortHandler =
    (field: string) => (event: React.MouseEvent<unknown>) => {
      setSortField(field);
      if (order === "asc") {
        setOrder("desc");
      } else {
        setOrder("asc");
      }
    };

  const [deleteCategory] = useMutation(DeleteCategory);

  const handleDelete = () => {
    selected.forEach((categoryId) => {
      deleteCategory({
        variables: {
          id: categoryId,
        },
        refetchQueries: [
          GetIncomes,
          CountIncomes,
          GetExpenses,
          CountExpenses,
          GetCategories,
          CountCategories,
          "GetMe",
          "GetExpensesSumByCategory",
        ],
      });
    });
    setSelected([]);
    setNumSelected(0);
  };

  const columns: readonly Column[] = [
    { id: "icon", label: "Icon", minWidth: 170, sortable: false },
    { id: "name", label: "Name", minWidth: 170, sortable: true },
    { id: "actions", label: "Actions", minWidth: 70, sortable: false },
  ];

  return (
    <Card className="categories-card" sx={{ boxShadow: 4 }}>
      <CardContent>
        <Typography fontWeight="bold" variant="h4">
          Categories
        </Typography>
        <Divider sx={{ mt: "1.5%", mb: "1.5%" }} />
        <Paper sx={{ width: "100%", overflow: "hidden" }}>
          <TableContainer sx={{ maxHeight: 370 }}>
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell padding="checkbox">
                    <Checkbox
                      color="primary"
                      indeterminate={
                        loading == false
                          ? numSelected > 0 &&
                            numSelected < data!.categoryList!.length
                          : false
                      }
                      checked={
                        loading == false
                          ? data!.categoryList!.length > 0 &&
                            numSelected === data!.categoryList!.length
                          : false
                      }
                      onChange={handleSelectAllClick}
                    />
                  </TableCell>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      <TableSortLabel
                        disabled={!column.sortable}
                        active={sortField === column.id}
                        direction={order}
                        onClick={createSortHandler(column.id)}
                      >
                        {column.label}
                      </TableSortLabel>
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {data?.categoryList!.map((category) => {
                  const isItemSelected = isSelected(category.id);
                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleItemClick(event, category.id)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={category.id}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox color="primary" checked={isItemSelected} />
                      </TableCell>
                      <TableCell>
                        <Icon sx={{ color: category.color, mr: "3%" }}>
                          {category.iconName}
                        </Icon>
                      </TableCell>
                      <TableCell>{category.name}</TableCell>
                      <TableCell>
                        <CategoryEditForm categoryId={category.id} />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 20]}
            component="div"
            count={countLoading == false ? countData!.categoriesNumber! : -1}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />

          <Toolbar
            className="income-table-toolbar"
            style={{
              backgroundColor: "#e2e2e2",
            }}
          >
            <Grid container spacing={3}>
              <Grid
                item
                xs={3}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <CategoryForm />
              </Grid>
              <Grid item xs>
                <Box sx={{ flexGrow: 1 }} />
              </Grid>
              <Grid
                item
                xs={3}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-end",
                }}
              >
                <p>{numSelected} items selected.</p>
                <IconButton onClick={handleDelete}>
                  <DeleteIcon />
                </IconButton>
              </Grid>
            </Grid>
          </Toolbar>
        </Paper>
      </CardContent>
    </Card>
  );
}
