import { Card, CardContent } from "@mui/material";
import { useUser } from "@providers/user";

export default function BalanceCard() {
  const user = useUser();

  return (
    <Card
      className="dashboard-balance-card"
      sx={{
        boxShadow: 4,
        width: "92%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "2%",
      }}
    >
      <CardContent sx={{ width: "100%" }}>
        <a style={{ fontSize: "150%", marginRight: "1%" }}>
          Your current balance:
        </a>
        {user.balance.amount < 0 ? (
          <a style={{ color: "red", fontWeight: "bold", fontSize: "130%" }}>
            {user.balance.amount.toFixed(2)} {user.settings.currencyType}
          </a>
        ) : (
          <a style={{ color: "#43a047", fontWeight: "bold", fontSize: "130%" }}>
            {user.balance.amount.toFixed(2)} {user.settings.currencyType}
          </a>
        )}
      </CardContent>
    </Card>
  );
}
