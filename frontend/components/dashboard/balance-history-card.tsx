import { useQuery } from "@apollo/client";
import { Card, CardContent, Typography } from "@mui/material";
import { useUser } from "@providers/user";
import { formatDateForChart, formatDateForChartLabel } from "@utils/methods";
import {
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Area,
  Tooltip,
} from "recharts";
import { GetBalanceHistories } from "./balance-history.query.gql";

export default function BalanceHistoryCard() {
  const user = useUser();

  const { data, loading } = useQuery(GetBalanceHistories, {
    variables: {
      userId: user.id,
      page: 0,
      size: 10,
      field: "balanceDate",
      sort: "asc",
    },
  });

  return (
    <Card
      className="dashboard-histories-card"
      sx={{
        boxShadow: 4,
        width: "92%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "3%",
      }}
    >
      <CardContent sx={{ width: "100%" }}>
        <Typography variant="h5" sx={{ mb: "1%" }}>
          Your balance history:
        </Typography>
        {!loading && (
          <AreaChart
            width={800}
            height={250}
            data={data!.balanceHistoryList!}
            margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
          >
            <defs>
              <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#76d275" stopOpacity={0.8} />
                <stop offset="95%" stopColor="#76d275" stopOpacity={0} />
              </linearGradient>
            </defs>
            <XAxis dataKey="balanceDate" tickFormatter={formatDateForChart} />
            <YAxis />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip labelFormatter={formatDateForChartLabel} />
            <Area
              type="monotone"
              dataKey="amount"
              stroke="#43a047"
              fillOpacity={1}
              fill="url(#colorUv)"
            />
          </AreaChart>
        )}
      </CardContent>
    </Card>
  );
}
