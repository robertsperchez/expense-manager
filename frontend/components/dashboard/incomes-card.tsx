import { useQuery } from "@apollo/client";
import { GetIncomes } from "@components/incomes/incomes.query.gql";
import { ArrowForward } from "@mui/icons-material";
import {
  Card,
  CardContent,
  Typography,
  IconButton,
  TableCell,
  Icon,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import { useUser } from "@providers/user";
import { formatStringDate } from "@utils/methods";
import { useNavigate } from "react-router-dom";
import { createRoute } from "../../routes";

interface Column {
  id: string;
  label: string;
  minWidth?: number;
}

export default function IncomesCard() {
  const user = useUser();
  const navigate = useNavigate();

  const columns: readonly Column[] = [
    { id: "title", label: "Title", minWidth: 20 },
    { id: "category", label: "Category", minWidth: 20 },
    { id: "amount", label: "Amount", minWidth: 120 },
    { id: "incomeDate", label: "Income Date", minWidth: 20 },
    { id: "account", label: "Account", minWidth: 170 },
  ];

  const { data, loading } = useQuery(GetIncomes, {
    variables: {
      userId: user.id,
      page: 0,
      size: 5,
      field: "incomeDate",
      sort: "desc",
    },
  });

  const handleIncomesRedirect = () => {
    navigate(createRoute.incomes());
  };

  return (
    <Card
      className="dashboard-incomes-card"
      sx={{
        boxShadow: 4,
        backgroundColor: "#76d275",
        width: "92%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "3%",
        marginBottom: "100px"
      }}
    >
      <CardContent sx={{ width: "100%" }}>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <Typography variant="h5" sx={{ mb: "1%", color: "white" }}>
            Latest Incomes:
          </Typography>
          <IconButton
            sx={{ width: "15%", ml: "auto", mb: "1%" }}
            onClick={handleIncomesRedirect}
          >
            <ArrowForward sx={{ color: "white" }} />
          </IconButton>
        </div>
        <Paper sx={{ width: "100%", overflow: "hidden" }}>
          <TableContainer sx={{ maxHeight: 220 }}>
            <Table size="small" stickyHeader>
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {data?.incomeList!.map((income) => {
                  return (
                    <TableRow hover tabIndex={-1} key={income.id}>
                      <TableCell>{income.title}</TableCell>
                      <TableCell sx={{ display: "flex" }}>
                        <Icon sx={{ color: income.category.color, mr: "3%" }}>
                          {income.category.iconName}
                        </Icon>
                        {income.category.name}
                      </TableCell>
                      <TableCell sx={{ color: "#43a047" }}>
                        {income.amount} {user.settings.currencyType}
                      </TableCell>
                      <TableCell>
                        {formatStringDate(income.incomeDate)}
                      </TableCell>
                      <TableCell>{income.account.name}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </CardContent>
    </Card>
  );
}
