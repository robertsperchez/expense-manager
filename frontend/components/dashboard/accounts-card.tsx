import { useQuery } from "@apollo/client";
import { GetAccountsPaginated } from "@components/accounts/accounts.query.gql";
import { ArrowForward, ArrowLeft } from "@mui/icons-material";
import {
  Card,
  CardContent,
  CircularProgress,
  Grid,
  IconButton,
  Typography,
} from "@mui/material";
import { useUser } from "@providers/user";
import { useNavigate } from "react-router-dom";
import { createRoute } from "../../routes";

export default function AccountsCard() {
  const user = useUser();
  const navigate = useNavigate();

  const { data, loading } = useQuery(GetAccountsPaginated, {
    variables: {
      userId: user.id,
      page: 0,
      size: 4,
      field: "name",
      sort: "asc",
    },
    nextFetchPolicy: "cache-and-network",
  });

  const handleAccountsRedirect = () => {
    navigate(createRoute.accounts());
  };

  return (
    <Card
      className="dashboard-accounts-card"
      sx={{
        boxShadow: 4,
        backgroundColor: "#76d275",
        width: "92%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "2%",
      }}
    >
      <CardContent sx={{ width: "100%" }}>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <Typography variant="h5" sx={{ mb: "1%", color: "white" }}>
            Your accounts:
          </Typography>
          <IconButton
            sx={{ width: "15%", ml: "auto", mb: "1%" }}
            onClick={handleAccountsRedirect}
          >
            <ArrowForward sx={{ color: "white" }} />
          </IconButton>
        </div>
        <Grid
          container
          spacing={{ xs: 0, md: 2 }}
          columns={{ xs: 2, sm: 2, md: 0 }}
        >
          {loading == false ? (
            data?.accountPaginatedList!.map((account) => {
              return (
                <Grid item xs={1} sm={1} md={1} key={account.id}>
                  <Card
                    className="accounts-info-card"
                    sx={{
                      boxShadow: 4,
                      backgroundImage:
                        "url('../resources/card-background.svg')",
                      backgroundAttachment: "fixed",
                      backgroundSize: "cover",
                      width: "100%",
                    }}
                  >
                    <CardContent>
                      <Typography variant="h6" sx={{ mt: "1%" }}>
                        {account.name}
                      </Typography>
                      <a>Amount: </a>
                      {account.amount < 0 ? (
                        <a style={{ color: "red", fontWeight: "bold" }}>
                          {account.amount.toFixed(2)}{" "}
                          {user.settings.currencyType}
                        </a>
                      ) : (
                        <a style={{ color: "#43a047", fontWeight: "bold" }}>
                          {account.amount.toFixed(2)}{" "}
                          {user.settings.currencyType}
                        </a>
                      )}
                    </CardContent>
                  </Card>
                </Grid>
              );
            })
          ) : (
            <CircularProgress />
          )}
        </Grid>
      </CardContent>
    </Card>
  );
}
