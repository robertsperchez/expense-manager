import { useQuery } from "@apollo/client";
import { GetExpenses } from "@components/expenses/expenses.query.gql";
import { ArrowForward } from "@mui/icons-material";
import {
  Card,
  CardContent,
  Typography,
  IconButton,
  TableCell,
  Icon,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import { useUser } from "@providers/user";
import { formatStringDate } from "@utils/methods";
import { useNavigate } from "react-router-dom";
import { createRoute } from "../../routes";

interface Column {
  id: string;
  label: string;
  minWidth?: number;
}

export default function ExpensesCard() {
  const user = useUser();
  const navigate = useNavigate();

  const columns: readonly Column[] = [
    { id: "title", label: "Title", minWidth: 20 },
    { id: "category", label: "Category", minWidth: 20 },
    { id: "amount", label: "Amount", minWidth: 120 },
    { id: "expenseDate", label: "Expense Date", minWidth: 20 },
    { id: "account", label: "Account", minWidth: 170 },
  ];

  const { data, loading } = useQuery(GetExpenses, {
    variables: {
      userId: user.id,
      page: 0,
      size: 5,
      field: "expenseDate",
      sort: "desc",
    },
  });

  const handleExpensesRedirect = () => {
    navigate(createRoute.expenses());
  };

  return (
    <Card
      className="dashboard-expenses-card"
      sx={{
        boxShadow: 4,
        backgroundColor: "#76d275",
        width: "92%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "3%",
      }}
    >
      <CardContent sx={{ width: "100%" }}>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <Typography variant="h5" sx={{ mb: "1%", color: "white" }}>
            Latest expenses:
          </Typography>
          <IconButton
            sx={{ width: "15%", ml: "auto", mb: "1%" }}
            onClick={handleExpensesRedirect}
          >
            <ArrowForward sx={{ color: "white" }} />
          </IconButton>
        </div>
        <Paper sx={{ width: "100%", overflow: "hidden" }}>
          <TableContainer sx={{ maxHeight: 220 }}>
            <Table size="small" stickyHeader>
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {data?.expenseList!.map((expense) => {
                  return (
                    <TableRow hover tabIndex={-1} key={expense.id}>
                      <TableCell>{expense.title}</TableCell>
                      <TableCell sx={{ display: "flex" }}>
                        <Icon sx={{ color: expense.category.color, mr: "3%" }}>
                          {expense.category.iconName}
                        </Icon>
                        {expense.category.name}
                      </TableCell>
                      <TableCell sx={{ color: "red" }}>
                        {expense.amount} {user.settings.currencyType}
                      </TableCell>
                      <TableCell>
                        {formatStringDate(expense.expenseDate)}
                      </TableCell>
                      <TableCell>{expense.account.name}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </CardContent>
    </Card>
  );
}
