import { useQuery } from "@apollo/client";
import { GetExpensesSumByCategory } from "@components/expenses/expenses.query.gql";
import { Card, CardContent, Typography } from "@mui/material";
import { useUser } from "@providers/user";
import { PieChart, Pie, Tooltip, Legend } from "recharts";

interface ExpenseSum {
  category: string;
  sum: number;
  fill: string;
}

function fromStringsToExpenseSum(strings: string[]): ExpenseSum[] {
  return strings.map((str) => {
    var split = str.split(",", 3);
    return {
      sum: parseFloat(split[0]),
      category: split[1],
      fill: split[2],
    };
  });
}

export default function ExpensesCategoryCard() {
  const { data, loading } = useQuery(GetExpensesSumByCategory);

  return (
    <Card
      className="dashboard-categories-card"
      sx={{
        boxShadow: 4,
        width: "92%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "3%",
      }}
    >
      <CardContent sx={{ width: "100%" }}>
        <Typography variant="h5" sx={{ mb: "1%" }}>
          Your expenses by category:
        </Typography>
          {!loading && (
            <PieChart width={730} height={290} style={{marginLeft:"auto", marginRight:"auto"}}>
              <Pie
                data={fromStringsToExpenseSum(data!.expensesByCategory!)}
                dataKey="sum"
                nameKey="category"
                cx="50%"
                cy="50%"
                outerRadius={100}
                label
              />
              <Tooltip />
              <Legend />
            </PieChart>
          )}
      </CardContent>
    </Card>
  );
}
