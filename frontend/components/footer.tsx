import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import Grid from "@mui/material/Grid";

export function AppFooter() {
    return (
        <AppBar position="fixed" sx={{ top: 'auto', bottom: 0 }} style={{
            backgroundColor: '#e2e2e2',
            height: 65
        }}>
            <Toolbar>
                <Grid container spacing={3} >
                    <Grid item xs={3} style={{
                        display: 'flex',
                        flexDirection: 'row'
                    }}>
                        <IconButton href="https://www.facebook.com/sperchez.robert" style={{
                            color: '#282828'
                        }}>
                            <FacebookIcon />
                        </IconButton>
                        <IconButton href="https://www.linkedin.com/in/robert-sperchez/" style={{
                            color: '#282828'
                        }}>
                            <LinkedInIcon />
                        </IconButton>
                    </Grid>
                    <Grid item xs>
                        <Box sx={{ flexGrow: 1 }} />
                    </Grid>
                    <Grid item xs={3}>
                        <div className="footer-logo-text">
                            <span className="sparrow-text">sparrow</span>
                            <p className="credits-text">a website by Sperchez Robert</p>
                        </div>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}