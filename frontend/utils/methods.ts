import { format } from "date-fns";

export function formatStringDate(dateString: string) {
    const date = new Date(dateString);
    return date.toLocaleDateString();
}

export function isValidDate(date: string) {
    var parsedDate = Date.parse(date);
    return (!isNaN(parsedDate));
}

export function formatDateForChart(date: string) {
    return format(new Date(date), 'dd/MM');
}

export function formatDateForChartLabel(date: string) {
    return new Date(date).toDateString();
}