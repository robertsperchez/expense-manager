import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { ReactNode } from 'react';

interface Props {
  children: ReactNode;
}

const theme = createTheme({
  palette: {
    primary: {
      main: '#43a047',
    },
    secondary: {
      main: '#76d275',
    },
  },
});

export function MaterialProvider(props: Props) {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {props.children}
    </ThemeProvider>
  );
}