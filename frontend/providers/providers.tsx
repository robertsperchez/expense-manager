import { ComponentType, ReactNode } from "react";
import { BrowserRouter } from "react-router-dom";
import { ApolloProvider } from "./apollo";
import { MaterialProvider } from "./material";
import { AuthProvider } from "./auth";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";

function Providers({ children }: { children: ReactNode }) {
  return (
    <ApolloProvider>
      <BrowserRouter>
        <MaterialProvider>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <AuthProvider>{children}</AuthProvider>
          </LocalizationProvider>
        </MaterialProvider>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export function withProviders<T>(Component: ComponentType<T>) {
  const ComponentWithProviders = (props: T) => (
    <Providers>
      <Component {...props} />
    </Providers>
  );

  const displayName = Component.displayName || Component.name || "Component";
  ComponentWithProviders.displayName = `withProviders(${displayName})`;

  return ComponentWithProviders;
}
