import { useQuery } from "@apollo/client";
import config from "@lib/config";
import { createContextProvider } from "@utils/context";
import { useCallback,  useState } from "react";
import { useNavigate } from "react-router-dom";
import { createRoute } from "../../routes";
import {GetMe} from "./me.gql";

export function useAuthStore() {
    const navigate = useNavigate();

    const {data, loading, error} = useQuery(GetMe, {
        fetchPolicy: "network-only",
    });

    const [loggedOut, setLoggedOut] = useState(false);

    const logout = useCallback(async () => {
        await fetch(config.api.baseUrl + "/auth/logout",{
            credentials: "include",
            mode: "cors",
        });
        setLoggedOut(true);
        navigate(createRoute.home());
    }, []);

    return {
        error,
        loading,
        loggedIn: !loggedOut && data?.me != null,
        user: data?.me,
        logout,
    }
}

export const [AuthProvider, useAuth] = createContextProvider( {
    name: "AuthProvider"
}, useAuthStore);