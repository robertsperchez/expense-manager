import { createContextProvider } from "@utils/context";
import { UserInfoFragment } from "../schemas/user.gql";

export const [UserProvider, useUser] = createContextProvider<UserInfoFragment>({
    name: "UserProvider"
});