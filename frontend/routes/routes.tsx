import { lazy, LazyExoticComponent, ReactNode } from "react";

export interface AppRoute {
  path: string;
  title?: string;
  requireAuth?: boolean;
  fallback?: NonNullable<ReactNode>;
  component: (() => JSX.Element) | LazyExoticComponent<() => JSX.Element>;
}

export const createRoute = {
  home: () => "/home",
  contact: () => "/contact",
  dashboard: () => "/dashboard",
  expenses: () => "/expenses",
  incomes: () => "/incomes",
  accounts: () => "/accounts",
  settings: () => "/settings",
  about: () => "/about",
};

export const routes: AppRoute[] = [
  {
    title: "Home",
    requireAuth: false,
    path: createRoute.home(),
    component: lazy(() => import("../pages/home")),
  },
  {
    title: "Contact",
    requireAuth: false,
    path: createRoute.contact(),
    component: lazy(() => import("../pages/contact")),
  },
  {
    title: "Dashboard",
    path: createRoute.dashboard(),
    component: lazy(() => import("../pages/dashboard")),
  },
  {
    title: "Expenses",
    path: createRoute.expenses(),
    component: lazy(() => import("../pages/expenses")),
  },
  {
    title: "Incomes",
    path: createRoute.incomes(),
    component: lazy(() => import("../pages/incomes")),
  },
  {
    title: "Accounts",
    path: createRoute.accounts(),
    component: lazy(() => import("../pages/accounts")),
  },
  {
    title: "Settings",
    path: createRoute.settings(),
    component: lazy(() => import("../pages/settings")),
  },
  {
    title: "About",
    requireAuth: false,
    path: createRoute.about(),
    component: lazy(() => import("../pages/about")),
  },
];
