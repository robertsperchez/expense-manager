package ro.sperchez.sparrow;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ro.sperchez.sparrow.openid.LoginController;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SparrowApplicationTests {

  @Autowired private LoginController loginController;

  @Test
  void contextLoads() {
    assertThat(loginController).isNotNull();
  }
}
