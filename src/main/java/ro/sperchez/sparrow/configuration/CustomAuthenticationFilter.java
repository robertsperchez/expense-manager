package ro.sperchez.sparrow.configuration;

import graphql.com.google.common.base.Strings;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.GenericFilterBean;
import ro.sperchez.sparrow.openid.AuthToken;

/**
 * The CustomAuthenticationFilter class is used as a custom filter for authentication instead of the
 * classic Spring implementation.
 */
@Slf4j
public class CustomAuthenticationFilter extends GenericFilterBean {

  @SneakyThrows
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    String token = getCookieToken(request);
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    String requestUri = httpRequest.getRequestURL().toString();

    if (requestUri.endsWith("/auth/login")
        || requestUri.endsWith("/auth/callback")
        || requestUri.endsWith("/auth/logout")
        // graphql for testing purposes
        //        || requestUri.endsWith("/graphql")
        || httpRequest.getMethod().equals("OPTIONS")) {
      log.info("Login request. Skipping authentication.");
      chain.doFilter(request, response);
      return;
    }
    if (Strings.isNullOrEmpty(token)) {
      log.error("No token found!");
      ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
      return;
    }

    try {
      AuthToken authToken = new AuthToken(token);
      request.setAttribute("userId", authToken.getUserId());
      request.setAttribute("googleSub", authToken.getGoogleSub());
      log.info("User authenticated with ID: " + authToken.getUserId());
    } catch (Exception e) {
      log.error("Error while authenticating user: " + e.getMessage());
      ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
      return;
    }

    chain.doFilter(request, response);
  }

  private String getCookieToken(ServletRequest servletRequest) {
    HttpServletRequest request = (HttpServletRequest) servletRequest;
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals("auth_token")) {
          return cookie.getValue();
        }
      }
    }
    return null;
  }
}
