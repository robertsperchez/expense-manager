package ro.sperchez.sparrow.configuration;

import com.zhokhov.graphql.datetime.LocalDateTimeScalar;
import graphql.schema.GraphQLScalarType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ScalarConfiguration {

  @Bean
  public GraphQLScalarType date() {
    return LocalDateTimeScalar.create(null, false, null);
  }
}
