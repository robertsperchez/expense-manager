package ro.sperchez.sparrow.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Expense;
import ro.sperchez.sparrow.service.ExpenseService;
import ro.sperchez.sparrow.util.Constants;

@Slf4j
@Component
public class ExpenseResolver implements GraphQLQueryResolver {

  @Autowired private ExpenseService expenseService;

  /**
   * Searches an expense by ID and then returns it.
   *
   * @param id The UUID used for searching the expense.
   * @return An expense.
   */
  public Expense expense(UUID id) {
    log.info("Retrieving expense by ID: {}", id);
    return expenseService.getExpenseById(id);
  }

  /**
   * Looks up a list of Expenses by User ID, while also including pagination.
   *
   * @param id The User ID used to search the expenses.
   * @param page Page number.
   * @param size Size of the page.
   * @param field Field to sort by.
   * @param sort Sort direction.
   * @return A paginated list of Expenses.
   */
  public List<Expense> expenseList(UUID id, int page, int size, String field, String sort) {
    log.info("Retrieving expense list for user ID: {}", id);
    Pageable pageable;
    if (Objects.nonNull(field) && Objects.nonNull(sort)) {
      pageable =
          sort.equals(Constants.DESC)
              ? PageRequest.of(page, size, Sort.by(field).descending())
              : PageRequest.of(page, size, Sort.by(field).ascending());
    } else {
      pageable = PageRequest.of(page, size);
    }
    return expenseService.findAllExpensesByUserId(id, pageable);
  }

  /**
   * Searches the number of expenses the user has.
   *
   * @param userId The UUID used for searching the expenses count.
   * @return The expense count.
   */
  public int expensesNumber(UUID userId) {
    log.info("Retrieving expense count by user ID: {}", userId);
    return expenseService.getExpensesCountByUserId(userId);
  }

  /**
   * Groups expenses by category and calculates the total amount of expenses for each category.
   *
   * @return A list of categories and their total amount of expenses.
   */
  public List<String> expensesByCategory() {
    return expenseService.calculateExpensesByCategory();
  }
}
