package ro.sperchez.sparrow.resolver.fields;

import graphql.kickstart.tools.GraphQLResolver;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Account;
import ro.sperchez.sparrow.entity.Expense;
import ro.sperchez.sparrow.entity.Income;
import ro.sperchez.sparrow.service.ExpenseService;
import ro.sperchez.sparrow.service.IncomeService;

@Slf4j
@Component
public class AccountFieldsResolver implements GraphQLResolver<Account> {

  @Autowired private IncomeService incomeService;

  @Autowired private ExpenseService expenseService;

  public List<Income> incomes(Account account) {
    log.info("Fetching incomes for account: {}", account.getId());
    return incomeService.findAllIncomesByAccountId(account.getId());
  }

  public List<Expense> expenses(Account account) {
    log.info("Fetching expenses for account: {}", account.getId());
    return expenseService.findAllExpensesByAccountId(account.getId());
  }
}
