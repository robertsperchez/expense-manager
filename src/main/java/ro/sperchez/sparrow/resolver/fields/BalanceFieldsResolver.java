package ro.sperchez.sparrow.resolver.fields;

import graphql.kickstart.tools.GraphQLResolver;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Balance;
import ro.sperchez.sparrow.entity.BalanceHistory;
import ro.sperchez.sparrow.service.BalanceHistoryService;

@Slf4j
@Component
public class BalanceFieldsResolver implements GraphQLResolver<Balance> {

  @Autowired private BalanceHistoryService balanceHistoryService;

  /**
   * Resolves the balanceHistories field of the balance query.
   *
   * @param balance The balance that needs fields resolving.
   * @return A list of balance histories.
   */
  public List<BalanceHistory> balanceHistories(Balance balance) {
    log.info("Fetching balance histories for balance: {}", balance.getId());
    return balanceHistoryService.findAllBalanceHistoriesByBalanceId(balance.getId());
  }
}
