package ro.sperchez.sparrow.resolver.fields;

import graphql.kickstart.tools.GraphQLResolver;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Category;
import ro.sperchez.sparrow.entity.Expense;
import ro.sperchez.sparrow.entity.Income;
import ro.sperchez.sparrow.service.ExpenseService;
import ro.sperchez.sparrow.service.IncomeService;

@Slf4j
@Component
public class CategoryFieldsResolver implements GraphQLResolver<Category> {

  @Autowired private IncomeService incomeService;

  @Autowired private ExpenseService expenseService;

  public List<Income> incomes(Category category) {
    log.info("Fetching incomes for category: {}", category.getId());
    return incomeService.findAllIncomesByCategoryId(category.getId());
  }

  public List<Expense> expenses(Category category) {
    log.info("Fetching expenses for category: {}", category.getId());
    return expenseService.findAllExpensesByCategoryId(category.getId());
  }
}
