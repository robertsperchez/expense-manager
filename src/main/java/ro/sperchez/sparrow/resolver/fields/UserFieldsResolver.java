package ro.sperchez.sparrow.resolver.fields;

import graphql.kickstart.tools.GraphQLResolver;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.*;
import ro.sperchez.sparrow.service.AccountService;
import ro.sperchez.sparrow.service.CategoryService;
import ro.sperchez.sparrow.service.ExpenseService;
import ro.sperchez.sparrow.service.IncomeService;

@Slf4j
@Component
public class UserFieldsResolver implements GraphQLResolver<User> {

  @Autowired private AccountService accountService;

  @Autowired private IncomeService incomeService;

  @Autowired private ExpenseService expenseService;

  @Autowired private CategoryService categoryService;

  public List<Account> accounts(User user) {
    log.info("Fetching accounts for user: {}", user.getId());
    return accountService.findAllAccountsByUserId(user.getId());
  }

  public List<Income> incomes(User user) {
    log.info("Fetching incomes for user: {}", user.getId());
    return incomeService.findAllIncomesByUserId(user.getId());
  }

  public List<Expense> expenses(User user) {
    log.info("Fetching expenses for user: {}", user.getId());
    return expenseService.findAllExpensesByUserId(user.getId());
  }

  public List<Category> categories(User user) {
    log.info("Fetching categories for user: {}", user.getId());
    return categoryService.findAllCategoriesByUserId(user.getId());
  }
}
