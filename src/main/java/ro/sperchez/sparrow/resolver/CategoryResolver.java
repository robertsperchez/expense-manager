package ro.sperchez.sparrow.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Category;
import ro.sperchez.sparrow.service.CategoryService;
import ro.sperchez.sparrow.util.Constants;

@Slf4j
@Component
public class CategoryResolver implements GraphQLQueryResolver {

  @Autowired private CategoryService categoryService;

  /**
   * Searches a category by ID and then returns it.
   *
   * @param id The UUID used for searching the category.
   * @return A category.
   */
  public Category category(UUID id) {
    log.info("Retrieving category by ID: {}", id);
    return categoryService.getCategoryById(id);
  }

  /**
   * Searches all categories by user ID and then returns them.
   *
   * @param userId The UUID used for searching the categories.
   * @param page Page number.
   * @param size Size of the page.
   * @param field Field to sort by.
   * @param sort Sort direction.
   * @return A list of categories.
   */
  public List<Category> categoryList(UUID userId, int page, int size, String field, String sort) {
    log.info("Retrieving category list for user: {}", userId);
    Pageable pageable;
    if (Objects.nonNull(field) && Objects.nonNull(sort)) {
      pageable =
          sort.equals(Constants.DESC)
              ? PageRequest.of(page, size, Sort.by(field).descending())
              : PageRequest.of(page, size, Sort.by(field).ascending());
    } else {
      pageable = PageRequest.of(page, size);
    }
    return categoryService.findAllCategoriesByUserId(userId, pageable);
  }

  /**
   * Searches the number of categories the user has.
   *
   * @param userId The UUID used for searching the category count.
   * @return The category count.
   */
  public int categoriesNumber(UUID userId) {
    log.info("Retrieving category count by user ID: {}", userId);
    return categoryService.getCategoryCountByUserId(userId);
  }
}
