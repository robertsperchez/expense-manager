package ro.sperchez.sparrow.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Account;
import ro.sperchez.sparrow.service.AccountService;
import ro.sperchez.sparrow.util.Constants;

@Slf4j
@Component
public class AccountResolver implements GraphQLQueryResolver {

  @Autowired private AccountService accountService;

  /**
   * Searches an account by ID and then returns it.
   *
   * @param id The UUID used for searching the account.
   * @return An account.
   */
  public Account account(UUID id) {
    log.info("Retrieving account by ID: {}", id);
    return accountService.getAccountById(id);
  }

  /**
   * Looks up a list of Accounts by User ID and then returns it.
   *
   * @param userId The User ID used to search the accounts.
   * @return A list of Accounts.
   */
  public List<Account> accountList(UUID userId) {
    log.info("Retrieving account list for user ID: {}", userId);
    return accountService.findAllAccountsByUserId(userId);
  }

  /**
   * Looks up a list of Accounts by User ID, while also including pagination.
   *
   * @param id The User ID used to search the accounts.
   * @param page Page number.
   * @param size Size of the page.
   * @param field Field to sort by.
   * @param sort Sort direction.
   * @return A paginated list of accounts.
   */
  public List<Account> accountPaginatedList(
      UUID id, int page, int size, String field, String sort) {
    log.info("Retrieving account list for user ID: {}", id);
    Pageable pageable;
    if (Objects.nonNull(field) && Objects.nonNull(sort)) {
      pageable =
          sort.equals(Constants.DESC)
              ? PageRequest.of(page, size, Sort.by(field).descending())
              : PageRequest.of(page, size, Sort.by(field).ascending());
    } else {
      pageable = PageRequest.of(page, size);
    }
    return accountService.findAllAccountsByUserId(id, pageable);
  }
}
