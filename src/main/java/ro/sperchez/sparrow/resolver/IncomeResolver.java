package ro.sperchez.sparrow.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Income;
import ro.sperchez.sparrow.service.IncomeService;
import ro.sperchez.sparrow.util.Constants;

@Slf4j
@Component
public class IncomeResolver implements GraphQLQueryResolver {

  @Autowired private IncomeService incomeService;

  /**
   * Searches an income by ID and then returns it.
   *
   * @param id The UUID used for searching the income.
   * @return An income.
   */
  public Income income(UUID id) {
    log.info("Retrieving income by ID: {}", id);
    return incomeService.getIncomeById(id);
  }

  /**
   * Looks up a list of Incomes by User ID, while also including pagination.
   *
   * @param id The User ID used to search the incomes.
   * @param page Page number.
   * @param size Size of the page.
   * @param field Field to sort by.
   * @param sort Sort direction.
   * @return A paginated list of Incomes.
   */
  public List<Income> incomeList(UUID id, int page, int size, String field, String sort) {
    log.info("Retrieving income list for user ID: {}", id);
    Pageable pageable;
    if (Objects.nonNull(field) && Objects.nonNull(sort)) {
      pageable =
          sort.equals(Constants.DESC)
              ? PageRequest.of(page, size, Sort.by(field).descending())
              : PageRequest.of(page, size, Sort.by(field).ascending());
    } else {
      pageable = PageRequest.of(page, size);
    }
    return incomeService.findAllIncomesByUserId(id, pageable);
  }

  /**
   * Searches the number of incomes the user has.
   *
   * @param userId The UUID used for searching the incomes.
   * @return The incomes count.
   */
  public int incomesNumber(UUID userId) {
    log.info("Retrieving income count by user ID: {}", userId);
    return incomeService.getIncomesCountByUserId(userId);
  }
}
