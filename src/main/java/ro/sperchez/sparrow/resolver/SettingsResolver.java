package ro.sperchez.sparrow.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Settings;
import ro.sperchez.sparrow.service.SettingsService;

@Slf4j
@Component
public class SettingsResolver implements GraphQLQueryResolver {

  @Autowired private SettingsService settingsService;

  /**
   * Searches settings by ID and then returns them.
   *
   * @param id The UUID used for searching the settings.
   * @return A Settings object.
   */
  public Settings settings(UUID id) {
    log.info("Retrieving settings by ID: {}", id);
    return settingsService.getSettingsById(id);
  }
}
