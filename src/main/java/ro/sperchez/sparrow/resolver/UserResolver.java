package ro.sperchez.sparrow.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import graphql.schema.DataFetchingEnvironment;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.service.UserService;
import ro.sperchez.sparrow.util.CustomGraphQLContext;

@Slf4j
@Component
public class UserResolver implements GraphQLQueryResolver {

  @Autowired private UserService userService;

  /**
   * Searches a user by ID and then returns it.
   *
   * @param id The UUID used for searching the user.
   * @return A user.
   */
  public User user(UUID id) {
    log.info("Retrieving user by ID: {}", id);
    return userService.getUserById(id);
  }

  /**
   * Returns the current logged-in user.
   *
   * @param env The data fetching environment.
   * @return The current logged-in user.
   */
  public User me(DataFetchingEnvironment env) {
    CustomGraphQLContext context = env.getContext();
    log.info("Retrieving user by ID: {}", context.getUserId());
    return userService.getUserById(context.getUserId());
  }
}
