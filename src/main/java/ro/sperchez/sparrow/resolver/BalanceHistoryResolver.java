package ro.sperchez.sparrow.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.BalanceHistory;
import ro.sperchez.sparrow.service.BalanceHistoryService;
import ro.sperchez.sparrow.util.Constants;

@Slf4j
@Component
public class BalanceHistoryResolver implements GraphQLQueryResolver {

  @Autowired private BalanceHistoryService balanceHistoryService;

  /**
   * Searches a balance history by ID and then returns it.
   *
   * @param id The UUID used for searching the balance history.
   * @return A balance history.
   */
  public BalanceHistory balanceHistory(UUID id) {
    log.info("Retrieving balance history by ID: {}", id);
    return balanceHistoryService.getBalanceHistoryById(id);
  }

  /**
   * Looks up a list of BalanceHistories by Balance ID, while also including pagination.
   *
   * @param id The Balance ID used to search the histories.
   * @param page Page number.
   * @param size Size of the page.
   * @param field Field to sort by.
   * @param sort Sort direction.
   * @return A paginated list of balance histories.
   */
  public List<BalanceHistory> balanceHistoryList(
      UUID id, int page, int size, String field, String sort) {
    log.info("Retrieving balance histories by balance ID: {}", id);
    Pageable pageable;
    if (Objects.nonNull(field) && Objects.nonNull(sort)) {
      pageable =
          sort.equals(Constants.DESC)
              ? PageRequest.of(page, size, Sort.by(field).descending())
              : PageRequest.of(page, size, Sort.by(field).ascending());
    } else {
      pageable = PageRequest.of(page, size);
    }
    return balanceHistoryService.findAllBalanceHistoriesByBalanceId(id, pageable);
  }
}
