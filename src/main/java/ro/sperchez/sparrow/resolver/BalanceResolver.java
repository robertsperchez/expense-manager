package ro.sperchez.sparrow.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Balance;
import ro.sperchez.sparrow.service.BalanceService;

@Slf4j
@Component
public class BalanceResolver implements GraphQLQueryResolver {

  @Autowired private BalanceService balanceService;

  /**
   * Searches for a balance by ID and then returns it.
   *
   * @param id The UUID used for searching for the balance.
   * @return A balance.
   */
  public Balance balance(UUID id) {
    log.info("Retrieving balance by ID: {}", id);
    return balanceService.getBalanceById(id);
  }
}
