package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Balance;
import ro.sperchez.sparrow.entity.input.BalanceInput;
import ro.sperchez.sparrow.service.BalanceService;

@Slf4j
@Component
public class BalanceMutation implements GraphQLMutationResolver {

  @Autowired private BalanceService balanceService;

  /**
   * Takes a BalanceInput to create a Balance, and then returns it.
   *
   * @param balanceInput The input object.
   * @return A Balance object.
   */
  public Balance balance(BalanceInput balanceInput) {
    log.info("Creating balance for user ID: {}", balanceInput.getUserId());
    return balanceService.createBalance(balanceInput);
  }

  /**
   * Deletes a balance from the database.
   *
   * @param id The ID of the balance to be deleted.
   */
  public UUID deleteBalance(UUID id) {
    log.info("Deleting balance with ID: {}", id);
    balanceService.deleteBalanceById(id);
    return id;
  }

  /**
   * Takes a BalanceInput to update a Balance, and then returns it.
   *
   * @param balanceInput The input object.
   * @return A Balance object.
   */
  public Balance updateBalance(BalanceInput balanceInput) {
    log.info("Updating balance with ID: {}", balanceInput.getId());
    return balanceService.updateBalance(balanceInput);
  }
}
