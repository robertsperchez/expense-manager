package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Expense;
import ro.sperchez.sparrow.entity.input.ExpenseInput;
import ro.sperchez.sparrow.service.ExpenseService;

@Slf4j
@Component
public class ExpenseMutation implements GraphQLMutationResolver {

  @Autowired private ExpenseService expenseService;

  /**
   * Takes an ExpenseInput to create an Expense, and then returns it.
   *
   * @param expenseInput The input object.
   * @return An expense object.
   */
  public Expense expense(ExpenseInput expenseInput) {
    log.info("Creating expense for user ID: {}", expenseInput.getUserId());
    return expenseService.createExpense(expenseInput);
  }

  /**
   * Deletes an expense from the database.
   *
   * @param id The ID of the expense to be deleted.
   */
  public UUID deleteExpense(UUID id) {
    log.info("Deleting expense with ID: {}", id);
    expenseService.deleteExpenseById(id);
    return id;
  }

  /**
   * Takes an ExpenseInput to update an Expense, and then returns it.
   *
   * @param expenseInput The input object.
   * @return An expense object.
   */
  public Expense updateExpense(ExpenseInput expenseInput) {
    log.info("Updating expense with ID: {}", expenseInput.getId());
    return expenseService.updateExpense(expenseInput);
  }
}
