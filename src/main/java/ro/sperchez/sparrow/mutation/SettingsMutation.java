package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Settings;
import ro.sperchez.sparrow.entity.input.SettingsInput;
import ro.sperchez.sparrow.service.SettingsService;

@Slf4j
@Component
public class SettingsMutation implements GraphQLMutationResolver {

  @Autowired private SettingsService settingsService;

  /**
   * Takes a SettingsInput to create Settings, and then returns them.
   *
   * @param settingsInput The input object.
   * @return A settings object.
   */
  public Settings settings(SettingsInput settingsInput) {
    log.info("Creating settings for user ID: {}", settingsInput.getUserId());
    return settingsService.createSettings(settingsInput);
  }

  /**
   * Deletes settings from the database.
   *
   * @param id The ID of the settings to be deleted.
   */
  public UUID deleteSettings(UUID id) {
    log.info("Deleting settings with ID: {}", id);
    settingsService.deleteSettingsById(id);
    return id;
  }

  /**
   * Takes a SettingsInput to update Settings, and then returns them.
   *
   * @param settingsInput The input object.
   * @return A settings object.
   */
  public Settings updateSettings(SettingsInput settingsInput) {
    log.info("Updating settings for user ID: {}", settingsInput.getUserId());
    return settingsService.updateSettings(settingsInput);
  }
}
