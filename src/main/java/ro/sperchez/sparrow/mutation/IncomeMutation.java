package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Income;
import ro.sperchez.sparrow.entity.input.IncomeInput;
import ro.sperchez.sparrow.service.IncomeService;

@Slf4j
@Component
public class IncomeMutation implements GraphQLMutationResolver {

  @Autowired private IncomeService incomeService;

  /**
   * Takes an IncomeInput to create an Income, and then returns it.
   *
   * @param incomeInput The input object.
   * @return An income object.
   */
  public Income income(IncomeInput incomeInput) {
    log.info("Creating income for user ID: {}", incomeInput.getUserId());
    return incomeService.createIncome(incomeInput);
  }

  /**
   * Deletes an income from the database.
   *
   * @param id The ID of the income to be deleted.
   */
  public UUID deleteIncome(UUID id) {
    log.info("Deleting income with ID: {}", id);
    incomeService.deleteIncomeById(id);
    return id;
  }

  /**
   * Takes an IncomeInput to update an Income, and then returns it.
   *
   * @param incomeInput The input object.
   * @return An income object.
   */
  public Income updateIncome(IncomeInput incomeInput) {
    log.info("Updating income with ID: {}", incomeInput.getId());
    return incomeService.updateIncome(incomeInput);
  }
}
