package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.entity.input.UserInput;
import ro.sperchez.sparrow.service.UserService;

@Slf4j
@Component
public class UserMutation implements GraphQLMutationResolver {

  @Autowired private UserService userService;

  /**
   * Takes a UserInput to create a User, and then returns it.
   *
   * @param userInput The input object.
   * @return A user object.
   */
  public User user(UserInput userInput) {
    log.info("Creating new user.");
    return userService.createUser(userInput);
  }

  /**
   * Deletes a user from the database.
   *
   * @param id The ID of the user to be deleted.
   */
  public UUID deleteUser(UUID id) {
    log.info("Deleting user with ID: {}", id);
    userService.deleteUserById(id);
    return id;
  }

  /**
   * Takes a UserInput to update a User, and then returns it.
   *
   * @param userInput The input object.
   * @return A user object.
   */
  public User updateUser(UserInput userInput) {
    log.info("Updating user with ID: {}", userInput.getId());
    return userService.updateUser(userInput);
  }
}
