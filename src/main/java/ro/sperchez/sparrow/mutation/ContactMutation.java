package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Contact;
import ro.sperchez.sparrow.service.EmailService;

@Slf4j
@Component
public class ContactMutation implements GraphQLMutationResolver {

  @Autowired private EmailService emailService;

  /**
   * Sends an email based on the contact form input.
   *
   * @param name The name of the sender.
   * @param email The email of the sender.
   * @param phoneNumber The phone number of the sender.
   * @param message The message of the sender.
   * @return The contact object.
   */
  public Contact contact(String name, String email, String phoneNumber, String message) {
    log.info("Sending contact email from {}.", email);
    Contact contactInfo =
        Contact.builder().name(name).email(email).phoneNumber(phoneNumber).message(message).build();
    emailService.sendEmail(contactInfo);
    return contactInfo;
  }
}
