package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Category;
import ro.sperchez.sparrow.entity.input.CategoryInput;
import ro.sperchez.sparrow.service.CategoryService;

@Slf4j
@Component
public class CategoryMutation implements GraphQLMutationResolver {

  @Autowired private CategoryService categoryService;

  /**
   * Takes a CategoryInput to create a Category, and then returns it.
   *
   * @param categoryInput The input object.
   * @return A category object.
   */
  public Category category(CategoryInput categoryInput) {
    log.info("Creating category for user ID: {}", categoryInput.getUserId());
    return categoryService.createCategory(categoryInput);
  }

  /**
   * Deletes a category from the database.
   *
   * @param id The ID of the category to be deleted.
   */
  public UUID deleteCategory(UUID id) {
    log.info("Deleting category with ID: {}", id);
    categoryService.deleteCategoryById(id);
    return id;
  }

  /**
   * Takes a CategoryInput to update a Category, and then returns it.
   *
   * @param categoryInput The input object.
   * @return A category object.
   */
  public Category updateCategory(CategoryInput categoryInput) {
    log.info("Updating category with ID: {}", categoryInput.getId());
    return categoryService.updateCategory(categoryInput);
  }
}
