package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.BalanceHistory;
import ro.sperchez.sparrow.entity.input.BalanceHistoryInput;
import ro.sperchez.sparrow.service.BalanceHistoryService;

@Slf4j
@Component
public class BalanceHistoryMutation implements GraphQLMutationResolver {

  @Autowired private BalanceHistoryService balanceHistoryService;

  /**
   * Takes a BalanceHistoryInput to create a BalanceHistory, and then returns it.
   *
   * @param balanceHistoryInput The input object.
   * @return A BalanceHistory object.
   */
  public BalanceHistory balanceHistory(BalanceHistoryInput balanceHistoryInput) {
    log.info("Creating balance history for user ID: {}", balanceHistoryInput.getBalanceId());
    return balanceHistoryService.createBalanceHistory(balanceHistoryInput);
  }

  /**
   * Deletes a balance history from the database.
   *
   * @param id The ID of the balance history to be deleted.
   */
  public UUID deleteBalanceHistory(UUID id) {
    log.info("Deleting balance history with ID: {}", id);
    balanceHistoryService.deleteBalanceHistoryById(id);
    return id;
  }

  /**
   * Takes a BalanceHistoryInput to update a BalanceHistory, and then returns it.
   *
   * @param balanceHistoryInput The input object.
   * @return A BalanceHistory object.
   */
  public BalanceHistory updateBalanceHistory(BalanceHistoryInput balanceHistoryInput) {
    log.info("Updating balance history with ID: {}", balanceHistoryInput.getId());
    return balanceHistoryService.updateBalanceHistory(balanceHistoryInput);
  }
}
