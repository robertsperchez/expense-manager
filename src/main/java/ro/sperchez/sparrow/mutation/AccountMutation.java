package ro.sperchez.sparrow.mutation;

import graphql.kickstart.tools.GraphQLMutationResolver;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sperchez.sparrow.entity.Account;
import ro.sperchez.sparrow.entity.input.AccountInput;
import ro.sperchez.sparrow.service.AccountService;

@Slf4j
@Component
public class AccountMutation implements GraphQLMutationResolver {

  @Autowired private AccountService accountService;

  /**
   * Takes an AccountInput to create an Account, and then returns it.
   *
   * @param accountInput The input object.
   * @return An account object.
   */
  public Account account(AccountInput accountInput) {
    log.info("Creating account for user ID: {}", accountInput.getUserId());
    return accountService.createAccount(accountInput);
  }

  /**
   * Deletes an account from the database.
   *
   * @param id The ID of the account to be deleted.
   */
  public UUID deleteAccount(UUID id) {
    log.info("Deleting account with ID: {}", id);
    accountService.deleteAccountById(id);
    return id;
  }

  /**
   * Takes an AccountInput to update an Account, and then returns it.
   *
   * @param accountInput The input object.
   * @return An account object.
   */
  public Account updateAccount(AccountInput accountInput) {
    log.info("Updating account with ID: {}", accountInput.getId());
    return accountService.updateAccount(accountInput);
  }
}
