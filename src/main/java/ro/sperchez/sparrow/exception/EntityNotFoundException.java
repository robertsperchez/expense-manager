package ro.sperchez.sparrow.exception;

import graphql.ErrorClassification;
import graphql.GraphQLError;
import graphql.language.SourceLocation;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Exception thrown when an entity is searched in a service by a repository but is not found,
 * affecting a service operation.
 */
public class EntityNotFoundException extends RuntimeException implements GraphQLError {

  private static final long serialVersionUID = -3943710438830973150L;

  private final String entityName;

  public EntityNotFoundException(String message, String entityName) {
    super(message);
    this.entityName = entityName;
  }

  @Override
  public List<SourceLocation> getLocations() {
    return Collections.emptyList();
  }

  @Override
  public ErrorClassification getErrorType() {
    return null;
  }

  @Override
  public Map<String, Object> getExtensions() {
    return Collections.singletonMap("entityName", entityName);
  }
}
