package ro.sperchez.sparrow.exception;

import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.kickstart.execution.error.GenericGraphQLError;
import graphql.kickstart.execution.error.GraphQLErrorHandler;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * DefaultGraphQLExceptionHandler is a class that implements GraphQLErrorHandler to handle all the
 * GraphQL exceptions that might occur and send a message as a response from a server, without
 * showing the logic/implementation.
 */
@Component
public class DefaultGraphQLExceptionHandler implements GraphQLErrorHandler {

  @Override
  public List<GraphQLError> processErrors(List<GraphQLError> list) {
    return list.stream().map(this::getNested).collect(Collectors.toList());
  }

  private GraphQLError getNested(GraphQLError error) {
    if (error instanceof ExceptionWhileDataFetching) {
      ExceptionWhileDataFetching exceptionError = (ExceptionWhileDataFetching) error;
      if (exceptionError.getException() instanceof GraphQLError) {
        return (GraphQLError) exceptionError.getException();
      }
    }
    return new GenericGraphQLError("Internal Server Error(s) while executing query.");
  }
}
