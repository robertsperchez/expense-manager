package ro.sperchez.sparrow.entity;

import java.time.LocalDateTime;
import java.util.Currency;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Settings extends BaseEntity {

  private static final long serialVersionUID = -1931843379251711563L;

  @Builder
  public Settings(
      UUID id, LocalDateTime createdAt, LocalDateTime updatedAt, User user, String currencyType) {
    super(id, createdAt, updatedAt);
    this.user = user;
    this.currencyType = currencyType;
  }

  @OneToOne(mappedBy = "settings", optional = false)
  private User user;

  @Column(name = "currency_type", nullable = false)
  private String currencyType;

  public void setCurrencyType(Currency currency) {
    this.currencyType = currency.getCurrencyCode();
  }

  public Currency getCurrencyType() {
    return Currency.getInstance(currencyType);
  }
}
