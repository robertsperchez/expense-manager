package ro.sperchez.sparrow.entity;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.*;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Account extends BaseEntity {

  private static final long serialVersionUID = 3511417416940824610L;

  @Builder
  public Account(
      UUID id,
      LocalDateTime createdAt,
      LocalDateTime updatedAt,
      User user,
      String name,
      double amount,
      Set<Income> incomes,
      Set<Expense> expenses) {
    super(id, createdAt, updatedAt);
    this.user = user;
    this.name = name;
    this.amount = amount;
    this.incomes = incomes;
    this.expenses = expenses;
  }

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "user_id")
  private User user;

  @Column(name = "name", nullable = false, length = 64)
  @Length(min = 1, max = 64)
  private String name;

  @Column(name = "amount", nullable = false)
  private double amount;

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "account", fetch = FetchType.LAZY)
  private Set<Income> incomes;

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "account", fetch = FetchType.LAZY)
  private Set<Expense> expenses;
}
