package ro.sperchez.sparrow.entity;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.*;
import javax.validation.constraints.Email;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User extends BaseEntity {

  private static final long serialVersionUID = 5277857869285753664L;

  @Builder
  public User(
      UUID id,
      LocalDateTime createdAt,
      LocalDateTime updatedAt,
      String givenName,
      String familyName,
      String email,
      String profilePicture,
      String sub,
      Set<Account> accounts,
      Set<Income> incomes,
      Set<Expense> expenses,
      Set<Category> categories,
      Settings settings,
      Balance balance) {
    super(id, createdAt, updatedAt);
    this.givenName = givenName;
    this.familyName = familyName;
    this.email = email;
    this.profilePicture = profilePicture;
    this.accounts = accounts;
    this.incomes = incomes;
    this.expenses = expenses;
    this.categories = categories;
    this.settings = settings;
    this.balance = balance;
    this.sub = sub;
  }

  @Column(name = "sub")
  private String sub;

  @Column(name = "given_name", nullable = false, length = 64)
  @Length(min = 2, max = 64)
  private String givenName;

  @Column(name = "family_name", nullable = false, length = 64)
  @Length(min = 2, max = 64)
  private String familyName;

  @Column(name = "email", nullable = false, length = 128, unique = true)
  @Email
  private String email;

  @Column(name = "profile_picture")
  private String profilePicture;

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
  private Set<Account> accounts;

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
  private Set<Income> incomes;

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
  private Set<Expense> expenses;

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
  private Set<Category> categories;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "id", referencedColumnName = "id")
  private Settings settings;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "id", referencedColumnName = "id")
  private Balance balance;
}
