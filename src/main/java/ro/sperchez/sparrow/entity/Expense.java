package ro.sperchez.sparrow.entity;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.*;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Expense extends BaseEntity {

  private static final long serialVersionUID = -6632317483203525117L;

  @Builder
  public Expense(
      UUID id,
      LocalDateTime createdAt,
      LocalDateTime updatedAt,
      User user,
      Account account,
      Category category,
      double amount,
      LocalDateTime expenseDate,
      String title,
      String comment) {
    super(id, createdAt, updatedAt);
    this.user = user;
    this.account = account;
    this.category = category;
    this.amount = amount;
    this.expenseDate = expenseDate;
    this.title = title;
    this.comment = comment;
  }

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "user_id")
  private User user;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "account_id")
  private Account account;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "category_id")
  private Category category;

  @Column(name = "amount", nullable = false)
  private double amount;

  @Column(name = "expense_date", nullable = false)
  private LocalDateTime expenseDate;

  @Column(name = "title", nullable = false, length = 128)
  @Length(min = 1, max = 128)
  private String title;

  @Column(name = "comment", length = 512)
  @Length(max = 512)
  private String comment;
}
