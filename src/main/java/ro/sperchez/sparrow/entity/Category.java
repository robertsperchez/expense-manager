package ro.sperchez.sparrow.entity;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.*;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Category extends BaseEntity {

  private static final long serialVersionUID = -3715593112467832217L;

  @Builder
  public Category(
      UUID id,
      LocalDateTime createdAt,
      LocalDateTime updatedAt,
      Set<Income> incomes,
      Set<Expense> expenses,
      User user,
      String iconName,
      String name,
      String color) {
    super(id, createdAt, updatedAt);
    this.incomes = incomes;
    this.expenses = expenses;
    this.user = user;
    this.iconName = iconName;
    this.name = name;
    this.color = color;
  }

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "category", fetch = FetchType.LAZY)
  private Set<Income> incomes;

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "category", fetch = FetchType.LAZY)
  private Set<Expense> expenses;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;

  @Column(name = "icon_name", nullable = false, length = 128, unique = true)
  @Length(min = 2, max = 128)
  private String iconName;

  @Column(name = "name", nullable = false, length = 64, unique = true)
  @Length(min = 2, max = 64)
  private String name;

  @Column(name = "color", nullable = false, length = 64, unique = true)
  @Length(min = 2, max = 64)
  private String color;
}
