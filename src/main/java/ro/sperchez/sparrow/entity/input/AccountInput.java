package ro.sperchez.sparrow.entity.input;

import java.util.UUID;
import lombok.Data;

@Data
public class AccountInput {
  private UUID id;
  private UUID userId;
  private String name;
  private Double amount;
}
