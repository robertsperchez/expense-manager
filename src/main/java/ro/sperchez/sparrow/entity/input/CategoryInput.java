package ro.sperchez.sparrow.entity.input;

import java.util.UUID;
import lombok.Data;

@Data
public class CategoryInput {
  private UUID id;
  private UUID userId;
  private String iconName;
  private String name;
  private String color;
}
