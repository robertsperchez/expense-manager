package ro.sperchez.sparrow.entity.input;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;

@Data
public class IncomeInput {
  private UUID id;
  private UUID userId;
  private UUID accountId;
  private UUID categoryId;
  private Double amount;
  private LocalDateTime incomeDate;
  private String title;
  private String comment;
}
