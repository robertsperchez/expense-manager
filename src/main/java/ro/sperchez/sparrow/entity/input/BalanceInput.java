package ro.sperchez.sparrow.entity.input;

import java.util.UUID;
import lombok.Data;

@Data
public class BalanceInput {
  private UUID id;
  private UUID userId;
  private Double amount;
}
