package ro.sperchez.sparrow.entity.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode()
@AllArgsConstructor
public class GoogleUser {
  private String sub;
  private String givenName;
  private String familyName;
  private String email;
  private String profilePicture;
}
