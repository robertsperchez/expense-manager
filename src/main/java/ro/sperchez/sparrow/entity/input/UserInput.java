package ro.sperchez.sparrow.entity.input;

import java.util.UUID;
import lombok.Data;

@Data
public class UserInput {
  private UUID id;
  private String givenName;
  private String familyName;
  private String email;
}
