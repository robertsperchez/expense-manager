package ro.sperchez.sparrow.entity.input;

import java.util.UUID;
import lombok.Data;

@Data
public class BalanceHistoryInput {
  private UUID id;
  private UUID balanceId;
  private Double amount;
}
