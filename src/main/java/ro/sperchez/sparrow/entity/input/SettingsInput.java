package ro.sperchez.sparrow.entity.input;

import java.util.Currency;
import java.util.UUID;
import lombok.Data;

@Data
public class SettingsInput {
  private UUID userId;
  private String currencyType;

  public void setCurrencyType(Currency currency) {
    this.currencyType = currency.getCurrencyCode();
  }

  public Currency getCurrencyType() {
    return Currency.getInstance(currencyType);
  }
}
