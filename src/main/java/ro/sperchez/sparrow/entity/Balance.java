package ro.sperchez.sparrow.entity;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Balance extends BaseEntity {

  private static final long serialVersionUID = -6342339364226324540L;

  @Builder
  public Balance(
      UUID id,
      LocalDateTime createdAt,
      LocalDateTime updatedAt,
      User user,
      double amount,
      Set<BalanceHistory> balanceHistories) {
    super(id, createdAt, updatedAt);
    this.user = user;
    this.amount = amount;
    this.balanceHistories = balanceHistories;
  }

  @OneToOne(mappedBy = "balance", optional = false)
  private User user;

  @Column(name = "amount", nullable = false)
  private double amount;

  @ToString.Exclude
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "balance", fetch = FetchType.LAZY)
  private Set<BalanceHistory> balanceHistories;
}
