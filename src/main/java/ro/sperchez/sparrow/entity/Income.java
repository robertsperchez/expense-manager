package ro.sperchez.sparrow.entity;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.*;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Income extends BaseEntity {

  private static final long serialVersionUID = -373401642647612991L;

  @Builder
  public Income(
      UUID id,
      LocalDateTime createdAt,
      LocalDateTime updatedAt,
      User user,
      Account account,
      Category category,
      double amount,
      LocalDateTime incomeDate,
      String title,
      String comment) {
    super(id, createdAt, updatedAt);
    this.user = user;
    this.account = account;
    this.category = category;
    this.amount = amount;
    this.incomeDate = incomeDate;
    this.title = title;
    this.comment = comment;
  }

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "user_id")
  private User user;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "account_id")
  private Account account;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "category_id")
  private Category category;

  @Column(name = "amount", nullable = false)
  private double amount;

  @Column(name = "income_date", nullable = false)
  private LocalDateTime incomeDate;

  @Column(name = "title", nullable = false, length = 128)
  @Length(min = 1, max = 128)
  private String title;

  @Column(name = "comment", length = 512)
  @Length(max = 512)
  private String comment;
}
