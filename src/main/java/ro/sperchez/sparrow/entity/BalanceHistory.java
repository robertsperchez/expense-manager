package ro.sperchez.sparrow.entity;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class BalanceHistory extends BaseEntity {

  private static final long serialVersionUID = -6400282528546333143L;

  @Builder
  public BalanceHistory(
      UUID id,
      LocalDateTime createdAt,
      LocalDateTime updatedAt,
      Balance balance,
      double amount,
      LocalDateTime balanceDate) {
    super(id, createdAt, updatedAt);
    this.balance = balance;
    this.amount = amount;
    this.balanceDate = balanceDate;
  }

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "balance_id")
  private Balance balance;

  @Column(name = "amount", nullable = false)
  private double amount;

  @Column(name = "balance_date", nullable = false)
  private LocalDateTime balanceDate;
}
