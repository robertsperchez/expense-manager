package ro.sperchez.sparrow.openid;

import static ro.sperchez.sparrow.util.Constants.GOOGLE_SUB;
import static ro.sperchez.sparrow.util.Constants.USER_ID;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.shaded.json.JSONObject;
import java.text.ParseException;
import java.util.Map;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuthToken {
  private UUID userId;
  private String googleSub;

  private static final String SIGNATURE = "dSgVkYp3s6v9y$B?E(H+MbQeThWmZq4t";

  /**
   * Creates a new AuthToken from a JWT.
   *
   * @param token the JWT.
   */
  public AuthToken(String token) throws JOSEException, ParseException {
    JWSObject jwt = JWSObject.parse(token);
    JWSVerifier verifier = new MACVerifier(SIGNATURE);
    jwt.verify(verifier);
    Map<String, Object> object = jwt.getPayload().toJSONObject();
    this.userId = UUID.fromString(object.get(USER_ID).toString());
    this.googleSub = object.get(GOOGLE_SUB).toString();
  }

  /**
   * Creates and encodes a new AuthToken.
   *
   * @return the AuthToken.
   */
  public String encode() throws JOSEException {
    JSONObject payloadJSON = new JSONObject();
    payloadJSON.put(USER_ID, userId.toString());
    payloadJSON.put(GOOGLE_SUB, googleSub);

    JWSHeader jwtHeader = new JWSHeader(JWSAlgorithm.HS256);
    Payload jwtPayload = new Payload(payloadJSON.toString());
    JWSObject jwt = new JWSObject(jwtHeader, jwtPayload);
    jwt.sign(new MACSigner(SIGNATURE));
    return jwt.serialize();
  }
}
