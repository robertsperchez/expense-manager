package ro.sperchez.sparrow.openid;

import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.openid.connect.sdk.*;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.token.OIDCTokens;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import ro.sperchez.sparrow.entity.input.GoogleUser;

@Slf4j
@UtilityClass
public class GoogleOpenIdHelper {
  private static final ClientID CLIENT_ID =
      new ClientID("470185889506-h56s95v5t0da4duapj9v3r1959fo0rmv.apps.googleusercontent.com");
  private static final String CLIENT_SECRET = "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";
  private static final String PROVIDER_URI = "https://accounts.google.com";
  private static final URI REDIRECT_URI = URI.create("http://localhost:8080/auth/callback");
  private static final Scope DEFAULT_SCOPES = new Scope("openid", "email", "profile");
  private static final ResponseType RESPONSE_TYPE = new ResponseType(ResponseType.Value.CODE);

  private static OIDCProviderMetadata providerMetadata;

  private static OIDCProviderMetadata getProviderMetadata()
      throws URISyntaxException, IOException, ParseException {
    if (GoogleOpenIdHelper.providerMetadata == null) {
      GoogleOpenIdHelper.providerMetadata = discoverIssuer();
    }
    return GoogleOpenIdHelper.providerMetadata;
  }

  /**
   * Gets the OpenID configuration from the Google OpenID provider.
   *
   * @return The OpenID configuration.
   */
  public static OIDCProviderMetadata discoverIssuer()
      throws URISyntaxException, IOException, ParseException {
    URI issuerURI = new URI(GoogleOpenIdHelper.PROVIDER_URI);
    URL configurationURL = issuerURI.resolve("/.well-known/openid-configuration").toURL();
    InputStream stream = configurationURL.openStream();
    String providerInfo;
    try (java.util.Scanner s = new java.util.Scanner(stream)) {
      providerInfo = s.useDelimiter("\\A").hasNext() ? s.next() : "";
    }
    return OIDCProviderMetadata.parse(providerInfo);
  }

  /**
   * Creates a new authentication request.
   *
   * @param state The state to be used in the authentication request.
   * @param nonce The nonce to be used in the authentication request.
   * @return The authentication request.
   */
  public static URI createAuthorizationURL(State state, Nonce nonce) {
    AuthenticationRequest request;
    try {
      request =
          new AuthenticationRequest(
              getProviderMetadata().getAuthorizationEndpointURI(),
              RESPONSE_TYPE,
              DEFAULT_SCOPES,
              CLIENT_ID,
              REDIRECT_URI,
              state,
              nonce);
    } catch (Exception e) {
      log.error("Error creating authorization URL: " + e.getMessage());
      return null;
    }
    return request.toURI();
  }

  /**
   * Gets the authorization code from the request URL.
   *
   * @param requestUrl The request URL.
   * @return The authorization code.
   */
  public static AuthorizationCode getAuthorizationCode(URI requestUrl) {
    AuthenticationResponse authenticationResponse;
    try {
      authenticationResponse = AuthenticationResponseParser.parse(requestUrl);
    } catch (Exception e) {
      log.error("Error parsing authentication response: " + e.getMessage());
      return null;
    }
    if (authenticationResponse instanceof AuthenticationErrorResponse) {
      log.error(
          "Could not get authorization code:"
              + ((AuthenticationErrorResponse) authenticationResponse)
                  .getErrorObject()
                  .getDescription());
      return null;
    }
    AuthenticationSuccessResponse successResponse =
        (AuthenticationSuccessResponse) authenticationResponse;
    return successResponse.getAuthorizationCode();
  }

  /**
   * Gets the tokens using an authorization code.
   *
   * @param authorizationCode The authorization code.
   * @return The tokens.
   */
  public static OIDCTokens getTokens(AuthorizationCode authorizationCode) {
    AuthorizationCodeGrant codeGrant = new AuthorizationCodeGrant(authorizationCode, REDIRECT_URI);
    ClientSecretBasic clientCredentials =
        new ClientSecretBasic(CLIENT_ID, new Secret(CLIENT_SECRET));
    TokenRequest tokenReq =
        new TokenRequest(providerMetadata.getTokenEndpointURI(), clientCredentials, codeGrant);
    HTTPResponse tokenHTTPResp;
    try {
      tokenHTTPResp = tokenReq.toHTTPRequest().send();
    } catch (SerializeException | IOException e) {
      log.error("Error while getting tokens: " + e.getMessage());
      return null;
    }

    TokenResponse tokenResponse;
    try {
      tokenResponse = OIDCTokenResponseParser.parse(tokenHTTPResp);
    } catch (ParseException e) {
      log.error("Error while parsing tokens: " + e.getMessage());
      return null;
    }
    if (tokenResponse instanceof TokenErrorResponse) {
      log.error(
          "Could not get tokens:"
              + (tokenResponse.toErrorResponse().getErrorObject().getDescription()));
      return null;
    }

    OIDCTokenResponse accessTokenResponse = (OIDCTokenResponse) tokenResponse;
    return accessTokenResponse.getOIDCTokens();
  }

  /**
   * Gets the user info using an access token.
   *
   * @param accessToken The access token.
   * @return The user info as a GoogleUser object.
   */
  public static GoogleUser getUserInfo(AccessToken accessToken) {
    UserInfoRequest userInfoRequest =
        new UserInfoRequest(providerMetadata.getUserInfoEndpointURI(), accessToken);
    HTTPResponse userInfoHTTPResp;
    try {
      userInfoHTTPResp = userInfoRequest.toHTTPRequest().send();
    } catch (SerializeException | IOException e) {
      log.error("Error getting user info: " + e.getMessage());
      return null;
    }

    UserInfoResponse userInfoResponse;
    try {
      userInfoResponse = UserInfoResponse.parse(userInfoHTTPResp);
    } catch (ParseException e) {
      log.error("Error parsing user info response: " + e.getMessage());
      return null;
    }

    if (userInfoResponse instanceof UserInfoErrorResponse) {
      log.error(
          "Could not get user info:"
              + ((UserInfoErrorResponse) userInfoResponse).getErrorObject().getDescription());
      return null;
    }

    UserInfoSuccessResponse successResponse = (UserInfoSuccessResponse) userInfoResponse;
    JSONObject claims = successResponse.getUserInfo().toJSONObject();
    String sub = claims.getAsString("sub");
    String email = claims.getAsString("email");
    String givenName = claims.getAsString("given_name");
    String familyName = claims.getAsString("family_name");
    String picture = claims.getAsString("picture");
    return new GoogleUser(sub, givenName, familyName, email, picture);
  }
}
