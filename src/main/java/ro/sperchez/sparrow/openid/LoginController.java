package ro.sperchez.sparrow.openid;

import static ro.sperchez.sparrow.util.Constants.*;

import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.openid.connect.sdk.Nonce;
import com.nimbusds.openid.connect.sdk.token.OIDCTokens;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Objects;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.entity.input.GoogleUser;
import ro.sperchez.sparrow.service.UserService;

@Slf4j
@Controller
@RequestMapping("auth")
public class LoginController {

  @Autowired private UserService userService;

  /**
   * Redirects to the Google OAuth2 login page.
   *
   * @param req The request.
   * @param res The response.
   */
  @GetMapping("/login")
  public void login(HttpServletRequest req, HttpServletResponse res) throws IOException {
    String backTo = req.getParameter(BACK_TO);
    Boolean silentLogin = Objects.equals(req.getParameter("silent"), "true");

    Nonce nonce = new Nonce();
    State state = createState(backTo, silentLogin);

    Cookie stateCookie = new Cookie(AUTH_STATE, state.getValue());
    Cookie nonceCookie = new Cookie(AUTH_NONCE, nonce.getValue());

    res.addCookie(stateCookie);
    res.addCookie(nonceCookie);

    URI redirectURI = GoogleOpenIdHelper.createAuthorizationURL(state, nonce);
    try {
      res.sendRedirect(redirectURI.toString());
    } catch (IOException e) {
      log.error("Error redirecting to: " + redirectURI + " with error message: " + e.getMessage());
      res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
  }

  /**
   * Handles the Google OAuth2 callback.
   *
   * @param req The request.
   * @param res The response.
   * @param stateCookie The state cookie.
   * @param nonceCookie The nonce cookie.
   */
  @GetMapping("/callback")
  public void callback(
      HttpServletRequest req,
      HttpServletResponse res,
      @CookieValue(AUTH_STATE) String stateCookie,
      @CookieValue(AUTH_NONCE) String nonceCookie)
      throws IOException {
    LoginState loginState = this.decodeState(stateCookie);
    URI requestUrl = null;
    try {
      requestUrl = new URI(req.getRequestURL() + "?" + req.getQueryString());
    } catch (URISyntaxException e) {
      log.error("Error while decoding request URL: " + e.getMessage());
      res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }

    AuthorizationCode authorizationCode = GoogleOpenIdHelper.getAuthorizationCode(requestUrl);
    if (Objects.isNull(authorizationCode)) {
      log.error("Authorization code is null!");
      res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
    OIDCTokens tokens = GoogleOpenIdHelper.getTokens(authorizationCode);
    if (Objects.isNull(tokens)) {
      log.error("Tokens are null!");
      res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
      return;
    }
    GoogleUser googleUser = GoogleOpenIdHelper.getUserInfo(tokens.getAccessToken());
    if (Objects.isNull(googleUser)) {
      log.error("User info is null!");
      res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
      return;
    }
    User user = userService.findOrCreate(googleUser);

    try {
      AuthToken authToken = new AuthToken(user.getId(), user.getSub());
      Cookie authTokenCookie = new Cookie("auth_token", authToken.encode());
      authTokenCookie.setSecure(false);
      authTokenCookie.setHttpOnly(true);
      authTokenCookie.setPath("/");
      res.addCookie(authTokenCookie);
      res.sendRedirect(loginState.getBackTo());
    } catch (Exception e) {
      log.error("Error while creating auth token:" + e.getMessage());
      res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
  }

  @GetMapping("/logout")
  public void logout(HttpServletRequest req, HttpServletResponse res) {
    Cookie authTokenCookie = new Cookie("auth_token", "");
    authTokenCookie.setSecure(false);
    authTokenCookie.setHttpOnly(true);
    authTokenCookie.setPath("/");

    res.addCookie(authTokenCookie);
    res.setStatus(201);
  }

  private State createState(String backTo, Boolean silentLogin) {
    String serializedState =
        new JSONObject().put(BACK_TO, backTo).put(SILENT_LOGIN, silentLogin).toString();
    String encodedState = Base64.getEncoder().encodeToString(serializedState.getBytes());
    return new State(encodedState);
  }

  private LoginState decodeState(String state) {
    String decodedState = new String(Base64.getDecoder().decode(state));
    JSONObject objectState = new JSONObject(decodedState);
    String backTo = objectState.getString(BACK_TO);
    Boolean silentLogin = objectState.getBoolean(SILENT_LOGIN);
    return new LoginState(backTo, silentLogin);
  }
}
