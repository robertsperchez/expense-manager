package ro.sperchez.sparrow.openid;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode()
public class LoginState {
  private String backTo;
  private Boolean silentLogin;
}
