package ro.sperchez.sparrow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SparrowApplication {

  public static void main(String[] args) {
    SpringApplication.run(SparrowApplication.class, args);
  }
}
