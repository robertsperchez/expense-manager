package ro.sperchez.sparrow.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sperchez.sparrow.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, UUID> {

  List<Category> findAllByUserId(UUID userId);

  List<Category> findAllByUserId(UUID userId, Pageable pageable);

  int countByUserId(UUID userId);
}
