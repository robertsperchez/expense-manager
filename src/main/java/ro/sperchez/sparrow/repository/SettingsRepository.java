package ro.sperchez.sparrow.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sperchez.sparrow.entity.Settings;

@Repository
public interface SettingsRepository extends JpaRepository<Settings, UUID> {

  Settings getByUserId(UUID id);
}
