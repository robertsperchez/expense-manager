package ro.sperchez.sparrow.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sperchez.sparrow.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
  User findBySub(String sub);
}
