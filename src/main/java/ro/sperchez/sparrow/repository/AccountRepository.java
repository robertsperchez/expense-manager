package ro.sperchez.sparrow.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.sperchez.sparrow.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, UUID> {

  List<Account> findAllByUserId(UUID userId);

  List<Account> findAllByUserId(UUID userId, Pageable pageable);

  @Query(
      value = "select Cast(user_id as varchar) from account where account.id = ?1",
      nativeQuery = true)
  String findUserIdByAccountId(UUID accountId);
}
