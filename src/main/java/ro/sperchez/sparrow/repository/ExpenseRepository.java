package ro.sperchez.sparrow.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.sperchez.sparrow.entity.Expense;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, UUID> {

  List<Expense> findAllByUserId(UUID userId);

  List<Expense> findAllByUserId(UUID userId, Pageable pageable);

  List<Expense> findAllByAccountId(UUID accountId);

  List<Expense> findAllByCategoryId(UUID accountId);

  int countByUserId(UUID userId);

  @Query(
      value =
          "select SUM(expense.amount), category.\"name\", category.color \n"
              + "from expense \n"
              + "inner join category on expense.category_id=category.id \n"
              + "group by category.\"name\", category.color \n"
              + "order by SUM(amount) desc",
      nativeQuery = true)
  List<String> calculateExpensesByCategory();
}
