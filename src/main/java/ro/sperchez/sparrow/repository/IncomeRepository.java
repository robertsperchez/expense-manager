package ro.sperchez.sparrow.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sperchez.sparrow.entity.Income;

@Repository
public interface IncomeRepository extends JpaRepository<Income, UUID> {

  List<Income> findAllByUserId(UUID userId);

  List<Income> findAllByUserId(UUID userId, Pageable pageable);

  List<Income> findAllByAccountId(UUID accountId);

  List<Income> findAllByCategoryId(UUID categoryId);

  int countByUserId(UUID userId);
}
