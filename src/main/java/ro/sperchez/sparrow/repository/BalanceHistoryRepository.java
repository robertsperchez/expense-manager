package ro.sperchez.sparrow.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sperchez.sparrow.entity.BalanceHistory;

@Repository
public interface BalanceHistoryRepository extends JpaRepository<BalanceHistory, UUID> {

  List<BalanceHistory> findAllByBalanceId(UUID balanceId, Pageable pageable);

  List<BalanceHistory> findAllByBalanceId(UUID balanceId);
}
