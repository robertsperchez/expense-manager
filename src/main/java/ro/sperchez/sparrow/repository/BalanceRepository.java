package ro.sperchez.sparrow.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sperchez.sparrow.entity.Balance;

@Repository
public interface BalanceRepository extends JpaRepository<Balance, UUID> {

  Balance getById(UUID id);
}
