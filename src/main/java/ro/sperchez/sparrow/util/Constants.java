package ro.sperchez.sparrow.util;

import lombok.experimental.UtilityClass;

/**
 * Constants is a utility class used for storing constants used all throughout the project to
 * improve code reusage.
 */
@UtilityClass
public class Constants {
  // Error messages
  public static final String ENTITY_NOT_FOUND_START = "We were unable to find the ";
  public static final String ENTITY_NOT_FOUND_END = " entity that was selected.";

  // Class names
  public static final String USER = "user";
  public static final String CATEGORY = "category";
  public static final String BALANCE = "balance";
  public static final String BALANCE_HISTORY = "balanceHistory";
  public static final String ACCOUNT = "account";
  public static final String EXPENSE = "expense";
  public static final String INCOME = "income";
  public static final String SETTINGS = "settings";

  // Others
  public static final String DESC = "desc";

  // OpenID Auth
  public static final String USER_ID = "userId";
  public static final String GOOGLE_SUB = "googleSub";
  public static final String BACK_TO = "backTo";
  public static final String SILENT_LOGIN = "silentLogin";
  public static final String AUTH_STATE = "auth_state";
  public static final String AUTH_NONCE = "auth_nonce";
}
