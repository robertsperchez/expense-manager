package ro.sperchez.sparrow.util;

import graphql.kickstart.execution.context.DefaultGraphQLContext;
import graphql.kickstart.servlet.context.DefaultGraphQLServletContext;
import graphql.kickstart.servlet.context.DefaultGraphQLWebSocketContext;
import graphql.kickstart.servlet.context.GraphQLServletContextBuilder;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;
import javax.websocket.server.HandshakeRequest;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class CustomGraphQLContextBuilder implements GraphQLServletContextBuilder {

  /**
   * Builds a new {@link CustomGraphQLContext} instance that has the logged user's id and sub.
   *
   * @param httpServletRequest The {@link HttpServletRequest} instance.
   * @param httpServletResponse The {@link HttpServletResponse} instance.
   * @return A new {@link CustomGraphQLContext} instance.
   */
  public CustomGraphQLContext build(
      HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
    UUID userId = (UUID) httpServletRequest.getAttribute("userId");
    String googleSub = (String) httpServletRequest.getAttribute("googleSub");

    DefaultGraphQLServletContext servletContext =
        DefaultGraphQLServletContext.createServletContext()
            .with(httpServletRequest)
            .with(httpServletResponse)
            .build();

    return new CustomGraphQLContext(userId, googleSub, servletContext);
  }

  @Override
  public DefaultGraphQLWebSocketContext build(Session session, HandshakeRequest handshakeRequest) {
    throw new IllegalStateException("Not implemented");
  }

  public DefaultGraphQLContext build() {
    throw new IllegalStateException("Not implemented");
  }
}
