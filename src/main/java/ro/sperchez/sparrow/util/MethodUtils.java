package ro.sperchez.sparrow.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.function.Consumer;
import lombok.experimental.UtilityClass;

/** MethodUtils is a utility class used for various helper methods. */
@UtilityClass
public class MethodUtils {

  public static LocalDateTime fromStringToDate(String date) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    return date != null ? LocalDateTime.parse(date, formatter) : null;
  }

  public static <V> void setIfNotNull(V value, Consumer<V> setter) {
    if (Objects.nonNull(value)) {
      setter.accept(value);
    }
  }
}
