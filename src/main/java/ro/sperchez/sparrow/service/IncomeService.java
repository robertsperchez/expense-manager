package ro.sperchez.sparrow.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.Account;
import ro.sperchez.sparrow.entity.Category;
import ro.sperchez.sparrow.entity.Income;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.entity.input.IncomeInput;
import ro.sperchez.sparrow.exception.EntityNotFoundException;
import ro.sperchez.sparrow.repository.AccountRepository;
import ro.sperchez.sparrow.repository.CategoryRepository;
import ro.sperchez.sparrow.repository.IncomeRepository;
import ro.sperchez.sparrow.repository.UserRepository;
import ro.sperchez.sparrow.util.Constants;
import ro.sperchez.sparrow.util.MethodUtils;

@Service
@Transactional
public class IncomeService {

  @Autowired private IncomeRepository incomeRepository;

  @Autowired private UserRepository userRepository;

  @Autowired private AccountRepository accountRepository;

  @Autowired private CategoryRepository categoryRepository;

  @Autowired private AccountService accountService;

  /**
   * Searches an income by ID and then returns it.
   *
   * @param id The UUID used for searching the income.
   * @return An income.
   */
  public Income getIncomeById(UUID id) {
    return incomeRepository.getById(id);
  }

  /**
   * Searches incomes by user ID and then returns them.
   *
   * @param userId The user UUID used for searching the incomes.
   * @return A list of incomes.
   */
  public List<Income> findAllIncomesByUserId(UUID userId) {
    return incomeRepository.findAllByUserId(userId);
  }

  /**
   * Searches incomes by account ID and then returns them.
   *
   * @param accountId The account UUID used for searching the incomes.
   * @return A list of incomes.
   */
  public List<Income> findAllIncomesByAccountId(UUID accountId) {
    return incomeRepository.findAllByAccountId(accountId);
  }

  /**
   * Searches incomes by category ID and then returns them.
   *
   * @param categoryId The category UUID used for searching the incomes.
   * @return A list of incomes.
   */
  public List<Income> findAllIncomesByCategoryId(UUID categoryId) {
    return incomeRepository.findAllByCategoryId(categoryId);
  }

  /**
   * Searches incomes by user ID and then returns them.
   *
   * @param userId The user UUID used for searching the incomes.
   * @param pageable Used for pagination.
   * @return A list of incomes.
   */
  public List<Income> findAllIncomesByUserId(UUID userId, Pageable pageable) {
    return incomeRepository.findAllByUserId(userId, pageable);
  }

  /**
   * Gets the incomes count by user ID.
   *
   * @param userId The user UUID used for searching the incomes.
   * @return The incomes count.
   */
  public int getIncomesCountByUserId(UUID userId) {
    return incomeRepository.countByUserId(userId);
  }

  /**
   * Takes an IncomeInput to create an Income, and then returns it.
   *
   * @param incomeInput The input object.
   * @return An income object.
   */
  public Income createIncome(IncomeInput incomeInput) {
    Optional<User> user = userRepository.findById(incomeInput.getUserId());
    Optional<Category> category = categoryRepository.findById(incomeInput.getCategoryId());
    Optional<Account> account = accountRepository.findById(incomeInput.getAccountId());
    if (user.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    if (category.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.CATEGORY + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    if (account.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.ACCOUNT + Constants.ENTITY_NOT_FOUND_END,
          Constants.ACCOUNT);
    }
    Income income =
        Income.builder()
            .id(UUID.randomUUID())
            .user(user.get())
            .account(account.get())
            .category(category.get())
            .amount(incomeInput.getAmount())
            .incomeDate(incomeInput.getIncomeDate())
            .title(incomeInput.getTitle())
            .comment(incomeInput.getComment())
            .build();
    accountService.updateAccountAndBalanceForIncome(income, null);
    return incomeRepository.save(income);
  }

  /**
   * Deletes an income from the database.
   *
   * @param id The ID of the income to be deleted.
   */
  public void deleteIncomeById(UUID id) {
    Optional<Income> income = incomeRepository.findById(id);
    if (income.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.INCOME + Constants.ENTITY_NOT_FOUND_END,
          Constants.INCOME);
    }
    accountService.updateAccountAndBalanceOnDeleteIncome(income.get());
    incomeRepository.deleteById(id);
  }

  /**
   * Takes an IncomeInput to update an Income, and then returns it.
   *
   * @param incomeInput The input object.
   * @return An income object.
   */
  public Income updateIncome(IncomeInput incomeInput) {
    if (!incomeRepository.existsById(incomeInput.getId())) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.INCOME + Constants.ENTITY_NOT_FOUND_END,
          Constants.INCOME);
    }
    Income income = incomeRepository.getById(incomeInput.getId());

    if (!Objects.isNull(incomeInput.getUserId())) {
      Optional<User> user = userRepository.findById(incomeInput.getUserId());
      if (user.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
            Constants.USER);
      }
      MethodUtils.setIfNotNull(user.get(), income::setUser);
    }

    if (!Objects.isNull(incomeInput.getAccountId())) {
      Optional<Account> account = accountRepository.findById(incomeInput.getAccountId());
      if (account.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.ACCOUNT + Constants.ENTITY_NOT_FOUND_END,
            Constants.ACCOUNT);
      }
      MethodUtils.setIfNotNull(account.get(), income::setAccount);
    }

    if (!Objects.isNull(incomeInput.getCategoryId())) {
      Optional<Category> category = categoryRepository.findById(incomeInput.getCategoryId());
      if (category.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.CATEGORY + Constants.ENTITY_NOT_FOUND_END,
            Constants.CATEGORY);
      }
      MethodUtils.setIfNotNull(category.get(), income::setCategory);
    }
    accountService.updateAccountAndBalanceForIncome(income, incomeInput);
    MethodUtils.setIfNotNull(incomeInput.getAmount(), income::setAmount);
    MethodUtils.setIfNotNull(incomeInput.getIncomeDate(), income::setIncomeDate);
    MethodUtils.setIfNotNull(incomeInput.getComment(), income::setComment);
    MethodUtils.setIfNotNull(incomeInput.getTitle(), income::setTitle);
    return incomeRepository.save(income);
  }
}
