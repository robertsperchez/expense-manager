package ro.sperchez.sparrow.service;

import graphql.com.google.common.base.Strings;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.Contact;

@Slf4j
@Service
public class EmailService {

  public static final String HOST = "smtp.gmail.com";
  public static final String PORT = "587";
  public static final String FROM = "sparrow.budgeting@gmail.com";
  public static final String BR = "<br/>\n";
  public static final String ARIAL = "<a style=\"font-family: Arial;\">";
  public static final String A = "</a>\n";

  private Properties buildEmailProperties() {
    Properties properties = System.getProperties();
    properties.setProperty("mail.smtp.host", HOST);
    properties.setProperty("mail.smtp.port", PORT);
    properties.setProperty("mail.smtp.starttls.enable", "true");
    properties.setProperty("mail.smtp.auth", "true");
    return properties;
  }

  private Authenticator getAuthenticator() {
    return new Authenticator() {
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(FROM, "SecretPassword1!");
      }
    };
  }

  private String getPhoneNumber(String phoneNumber) {
    return Strings.isNullOrEmpty(phoneNumber) ? "N/A" : phoneNumber;
  }

  /**
   * Send email to the support email as well as the person contacting the support.
   *
   * @param contactInfo contact info
   */
  public void sendEmail(Contact contactInfo) {
    Properties emailProperties = buildEmailProperties();

    Session session = Session.getInstance(emailProperties, getAuthenticator());
    session.setDebug(true);

    try {
      MimeMessage message = new MimeMessage(session);
      message.setFrom(new InternetAddress(FROM));
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(FROM));
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(contactInfo.getEmail()));
      message.setSubject("Sparrow Budgeting - Contact (Sender: " + contactInfo.getName() + ")");
      message.setContent(
          "<p style=\"font-family: Arial;\">Hello! There is a new contact request from Sparrow's contact section!</p>\n"
              + "<p style=\"font-family: Arial;\">Here are the details:</p>\n"
              + "<a style=\"font-family: Arial; color: #43a047; font-weight: bold;\">Name: </a>\n"
              + ARIAL
              + contactInfo.getName()
              + A
              + BR
              + "<a style=\"font-family: Arial; color: #43a047; font-weight: bold;\">Email address: </a>\n"
              + ARIAL
              + contactInfo.getEmail()
              + A
              + BR
              + "<a style=\"font-family: Arial; color: #43a047; font-weight: bold;\">Phone number: </a>\n"
              + ARIAL
              + getPhoneNumber(contactInfo.getPhoneNumber())
              + A
              + BR
              + "<a style=\"font-family: Arial; color: #43a047; font-weight: bold;\">Message: </a>\n"
              + ARIAL
              + contactInfo.getMessage()
              + A
              + "<p style=\"font-family: Arial;\">Someone will get in touch soon. Thank you for using Sparrow!</p>",
          "text/html");
      Transport.send(message);
      log.info("Email sent to: " + contactInfo.getEmail());
    } catch (MessagingException mex) {
      mex.printStackTrace();
    }
  }
}
