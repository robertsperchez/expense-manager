package ro.sperchez.sparrow.service;

import java.time.LocalDateTime;
import java.util.*;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.Balance;
import ro.sperchez.sparrow.entity.BalanceHistory;
import ro.sperchez.sparrow.entity.input.BalanceHistoryInput;
import ro.sperchez.sparrow.exception.EntityNotFoundException;
import ro.sperchez.sparrow.repository.BalanceHistoryRepository;
import ro.sperchez.sparrow.repository.BalanceRepository;
import ro.sperchez.sparrow.util.Constants;
import ro.sperchez.sparrow.util.MethodUtils;

@Service
@Transactional
public class BalanceHistoryService {

  @Autowired private BalanceHistoryRepository balanceHistoryRepository;

  @Autowired private BalanceRepository balanceRepository;

  /**
   * Searches a balance history by ID and then returns it.
   *
   * @param id The UUID used for searching the balance history.
   * @return A balance history.
   */
  public BalanceHistory getBalanceHistoryById(UUID id) {
    return balanceHistoryRepository.getById(id);
  }

  /**
   * Searches balance histories by balance ID and then returns them.
   *
   * @param balanceId The balance UUID used for searching the balance histories.
   * @param pageable Used for pagination.
   * @return A list of balance histories.
   */
  public List<BalanceHistory> findAllBalanceHistoriesByBalanceId(
      UUID balanceId, Pageable pageable) {
    return balanceHistoryRepository.findAllByBalanceId(balanceId, pageable);
  }

  /**
   * Searches balance histories by balance ID and then returns them.
   *
   * @param balanceId The balance UUID used for searching the balance histories.
   * @return A list of balance histories.
   */
  public List<BalanceHistory> findAllBalanceHistoriesByBalanceId(UUID balanceId) {
    return balanceHistoryRepository.findAllByBalanceId(balanceId);
  }

  /**
   * Takes a BalanceHistoryInput to create a BalanceHistory, and then returns it.
   *
   * @param balanceHistoryInput The input object.
   * @return A BalanceHistory object.
   */
  public BalanceHistory createBalanceHistory(BalanceHistoryInput balanceHistoryInput) {
    Optional<Balance> balance = balanceRepository.findById(balanceHistoryInput.getBalanceId());
    if (balance.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.BALANCE + Constants.ENTITY_NOT_FOUND_END,
          Constants.BALANCE);
    }
    return balanceHistoryRepository.save(
        BalanceHistory.builder()
            .id(UUID.randomUUID())
            .balance(balance.get())
            .amount(balanceHistoryInput.getAmount())
            .balanceDate(LocalDateTime.now())
            .build());
  }

  /**
   * Deletes a balance history from the database.
   *
   * @param id The ID of the balance history to be deleted.
   */
  public void deleteBalanceHistoryById(UUID id) {
    balanceHistoryRepository.deleteById(id);
  }

  /**
   * Takes a BalanceHistoryInput to create a BalanceHistory, and then returns it.
   *
   * @param balanceHistoryInput The input object.
   * @return A BalanceHistory object.
   */
  public BalanceHistory updateBalanceHistory(BalanceHistoryInput balanceHistoryInput) {
    if (!balanceHistoryRepository.existsById(balanceHistoryInput.getId())) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START
              + Constants.BALANCE_HISTORY
              + Constants.ENTITY_NOT_FOUND_END,
          Constants.BALANCE_HISTORY);
    }
    BalanceHistory balanceHistory = balanceHistoryRepository.getById(balanceHistoryInput.getId());
    if (!Objects.isNull(balanceHistoryInput.getBalanceId())) {
      Optional<Balance> balance = balanceRepository.findById(balanceHistoryInput.getBalanceId());
      if (balance.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.BALANCE + Constants.ENTITY_NOT_FOUND_END,
            Constants.BALANCE);
      }
      MethodUtils.setIfNotNull(balance.get(), balanceHistory::setBalance);
    }
    MethodUtils.setIfNotNull(balanceHistoryInput.getAmount(), balanceHistory::setAmount);
    return balanceHistoryRepository.save(balanceHistory);
  }

  /** This method runs daily to update the balance histories of each user automatically. */
  @Scheduled(fixedRate = 86400000)
  public void dailyUpdateBalanceHistories() {
    List<Balance> currentBalances = balanceRepository.findAll();
    List<BalanceHistory> balanceHistories = new ArrayList<>();
    for (Balance balance : currentBalances) {
      balanceHistories.add(
          BalanceHistory.builder()
              .id(UUID.randomUUID())
              .balance(balance)
              .amount(balance.getAmount())
              .balanceDate(LocalDateTime.now())
              .build());
    }
    balanceHistoryRepository.saveAll(balanceHistories);
  }
}
