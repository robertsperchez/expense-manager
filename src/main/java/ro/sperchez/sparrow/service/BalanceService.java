package ro.sperchez.sparrow.service;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.Balance;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.entity.input.BalanceInput;
import ro.sperchez.sparrow.exception.EntityNotFoundException;
import ro.sperchez.sparrow.repository.BalanceRepository;
import ro.sperchez.sparrow.repository.UserRepository;
import ro.sperchez.sparrow.util.Constants;
import ro.sperchez.sparrow.util.MethodUtils;

@Service
@Transactional
public class BalanceService {

  @Autowired private BalanceRepository balanceRepository;

  @Autowired private UserRepository userRepository;

  /**
   * Searches for a balance by ID and then returns it.
   *
   * @param id The UUID used for searching for the balance.
   * @return A balance.
   */
  public Balance getBalanceById(UUID id) {
    return balanceRepository.getById(id);
  }

  /**
   * Takes a user to create a Balance for that user, and then returns the balance for them to be
   * used in the user creation.
   *
   * @param user The user that will be created with this balance.
   * @return The balance created.
   */
  public Balance createBalanceForNewUser(User user) {
    return balanceRepository.save(Balance.builder().id(user.getId()).user(user).amount(0).build());
  }

  /**
   * Takes a BalanceInput to create a Balance, and then returns it.
   *
   * @param balanceInput The input object.
   * @return A Balance object.
   */
  public Balance createBalance(BalanceInput balanceInput) {
    Optional<User> user = userRepository.findById(balanceInput.getUserId());
    if (user.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    return balanceRepository.save(
        Balance.builder()
            .id(balanceInput.getUserId())
            .user(user.get())
            .amount(balanceInput.getAmount())
            .build());
  }

  /**
   * Deletes a balance from the database.
   *
   * @param id The ID of the balance to be deleted.
   */
  public void deleteBalanceById(UUID id) {
    balanceRepository.deleteById(id);
  }

  /**
   * Takes a BalanceInput to update a Balance, and then returns it.
   *
   * @param balanceInput The input object.
   * @return A Balance object.
   */
  public Balance updateBalance(BalanceInput balanceInput) {
    if (!balanceRepository.existsById(balanceInput.getId())) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.BALANCE + Constants.ENTITY_NOT_FOUND_END,
          Constants.BALANCE);
    }
    Balance balance = balanceRepository.getById(balanceInput.getId());
    if (!Objects.isNull(balanceInput.getUserId())) {
      Optional<User> user = userRepository.findById(balanceInput.getUserId());
      if (user.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
            Constants.USER);
      }
      MethodUtils.setIfNotNull(user.get(), balance::setUser);
    }
    MethodUtils.setIfNotNull(balanceInput.getAmount(), balance::setAmount);
    return balanceRepository.save(balance);
  }
}
