package ro.sperchez.sparrow.service;

import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.Settings;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.entity.input.SettingsInput;
import ro.sperchez.sparrow.exception.EntityNotFoundException;
import ro.sperchez.sparrow.repository.SettingsRepository;
import ro.sperchez.sparrow.repository.UserRepository;
import ro.sperchez.sparrow.util.Constants;
import ro.sperchez.sparrow.util.MethodUtils;

@Service
@Transactional
public class SettingsService {

  @Autowired private SettingsRepository settingsRepository;

  @Autowired private UserRepository userRepository;

  /**
   * Searches settings by ID and then returns them.
   *
   * @param id The UUID used for searching the settings.
   * @return A Settings object.
   */
  public Settings getSettingsById(UUID id) {
    return settingsRepository.getByUserId(id);
  }

  /**
   * Takes a SettingsInput to create Settings, and then returns them.
   *
   * @param settingsInput The input object.
   * @return A settings object.
   */
  public Settings createSettings(SettingsInput settingsInput) {
    Optional<User> user = userRepository.findById(settingsInput.getUserId());
    if (user.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    return settingsRepository.save(
        Settings.builder()
            .id(settingsInput.getUserId())
            .user(user.get())
            .currencyType(settingsInput.getCurrencyType().getCurrencyCode())
            .build());
  }

  /**
   * Takes a user to create Settings for that user, and then returns the settings for them to be
   * used in the user creation.
   *
   * @param user The user that will be created with these settings.
   * @return The settings created.
   */
  public Settings createSettingsForNewUser(User user) {
    return settingsRepository.save(
        Settings.builder().id(user.getId()).user(user).currencyType("EUR").build());
  }

  /**
   * Deletes settings from the database.
   *
   * @param id The ID of the settings to be deleted.
   */
  public void deleteSettingsById(UUID id) {
    settingsRepository.deleteById(id);
  }

  /**
   * Takes a SettingsInput to update Settings, and then returns them.
   *
   * @param settingsInput The input object.
   * @return A settings object.
   */
  public Settings updateSettings(SettingsInput settingsInput) {
    if (!settingsRepository.existsById(settingsInput.getUserId())) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.SETTINGS + Constants.ENTITY_NOT_FOUND_END,
          Constants.SETTINGS);
    }
    Settings settings = settingsRepository.getById(settingsInput.getUserId());
    MethodUtils.setIfNotNull(settingsInput.getCurrencyType(), settings::setCurrencyType);
    return settingsRepository.save(settings);
  }
}
