package ro.sperchez.sparrow.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.Account;
import ro.sperchez.sparrow.entity.Category;
import ro.sperchez.sparrow.entity.Expense;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.entity.input.ExpenseInput;
import ro.sperchez.sparrow.exception.EntityNotFoundException;
import ro.sperchez.sparrow.repository.*;
import ro.sperchez.sparrow.util.Constants;
import ro.sperchez.sparrow.util.MethodUtils;

@Service
@Transactional
public class ExpenseService {

  @Autowired private ExpenseRepository expenseRepository;

  @Autowired private UserRepository userRepository;

  @Autowired private AccountRepository accountRepository;

  @Autowired private CategoryRepository categoryRepository;

  @Autowired private AccountService accountService;

  /**
   * Searches an expense by ID and then returns it.
   *
   * @param id The UUID used for searching the expense.
   * @return An expense.
   */
  public Expense getExpenseById(UUID id) {
    return expenseRepository.getById(id);
  }

  /**
   * Searches expenses by user ID and then returns them.
   *
   * @param userId The user UUID used for searching the expenses.
   * @return A list of expenses.
   */
  public List<Expense> findAllExpensesByUserId(UUID userId) {
    return expenseRepository.findAllByUserId(userId);
  }

  /**
   * Searches expenses by user ID and then returns them.
   *
   * @param userId The user UUID used for searching the expenses.
   * @param pageable Used for pagination.
   * @return A list of expenses.
   */
  public List<Expense> findAllExpensesByUserId(UUID userId, Pageable pageable) {
    return expenseRepository.findAllByUserId(userId, pageable);
  }

  /**
   * Groups expenses by category and calculates the total amount of expenses for each category.
   *
   * @return A list of categories and their total amount of expenses.
   */
  public List<String> calculateExpensesByCategory() {
    return expenseRepository.calculateExpensesByCategory();
  }

  /**
   * Gets the expenses count by user ID.
   *
   * @param userId The user UUID used for searching the expenses.
   * @return The expenses count.
   */
  public int getExpensesCountByUserId(UUID userId) {
    return expenseRepository.countByUserId(userId);
  }

  /**
   * Searches expenses by account ID and then returns them.
   *
   * @param accountId The account UUID used for searching the expenses.
   * @return A list of expenses.
   */
  public List<Expense> findAllExpensesByAccountId(UUID accountId) {
    return expenseRepository.findAllByAccountId(accountId);
  }

  /**
   * Searches expenses by category ID and then returns them.
   *
   * @param categoryId The category UUID used for searching the expenses.
   * @return A list of expenses.
   */
  public List<Expense> findAllExpensesByCategoryId(UUID categoryId) {
    return expenseRepository.findAllByCategoryId(categoryId);
  }

  /**
   * Takes an ExpenseInput to create an Expense, and then returns it.
   *
   * @param expenseInput The input object.
   * @return An expense object.
   */
  public Expense createExpense(ExpenseInput expenseInput) {
    Optional<User> user = userRepository.findById(expenseInput.getUserId());
    Optional<Category> category = categoryRepository.findById(expenseInput.getCategoryId());
    Optional<Account> account = accountRepository.findById(expenseInput.getAccountId());
    if (user.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    if (category.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.CATEGORY + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    if (account.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.ACCOUNT + Constants.ENTITY_NOT_FOUND_END,
          Constants.ACCOUNT);
    }
    Expense expense =
        Expense.builder()
            .id(UUID.randomUUID())
            .user(user.get())
            .account(account.get())
            .category(category.get())
            .amount(expenseInput.getAmount())
            .expenseDate(expenseInput.getExpenseDate())
            .title(expenseInput.getTitle())
            .comment(expenseInput.getComment())
            .build();
    accountService.updateAccountAndBalanceForExpense(expense, null);
    return expenseRepository.save(expense);
  }

  /**
   * Deletes an expense from the database.
   *
   * @param id The ID of the expense to be deleted.
   */
  public void deleteExpenseById(UUID id) {
    Optional<Expense> expense = expenseRepository.findById(id);
    if (expense.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.EXPENSE + Constants.ENTITY_NOT_FOUND_END,
          Constants.EXPENSE);
    }
    accountService.updateAccountAndBalanceOnDeleteExpense(expense.get());
    expenseRepository.deleteById(id);
  }

  /**
   * Takes an ExpenseInput to update an Expense, and then returns it.
   *
   * @param expenseInput The input object.
   * @return An expense object.
   */
  public Expense updateExpense(ExpenseInput expenseInput) {
    if (!expenseRepository.existsById(expenseInput.getId())) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.EXPENSE + Constants.ENTITY_NOT_FOUND_END,
          Constants.EXPENSE);
    }
    Expense expense = expenseRepository.getById(expenseInput.getId());

    if (!Objects.isNull(expenseInput.getUserId())) {
      Optional<User> user = userRepository.findById(expenseInput.getUserId());
      if (user.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
            Constants.USER);
      }
      MethodUtils.setIfNotNull(user.get(), expense::setUser);
    }

    if (!Objects.isNull(expenseInput.getAccountId())) {
      Optional<Account> account = accountRepository.findById(expenseInput.getAccountId());
      if (account.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.ACCOUNT + Constants.ENTITY_NOT_FOUND_END,
            Constants.ACCOUNT);
      }
      MethodUtils.setIfNotNull(account.get(), expense::setAccount);
    }

    if (!Objects.isNull(expenseInput.getCategoryId())) {
      Optional<Category> category = categoryRepository.findById(expenseInput.getCategoryId());
      if (category.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.CATEGORY + Constants.ENTITY_NOT_FOUND_END,
            Constants.CATEGORY);
      }
      MethodUtils.setIfNotNull(category.get(), expense::setCategory);
    }
    accountService.updateAccountAndBalanceForExpense(expense, expenseInput);
    MethodUtils.setIfNotNull(expenseInput.getAmount(), expense::setAmount);
    MethodUtils.setIfNotNull(expenseInput.getExpenseDate(), expense::setExpenseDate);
    MethodUtils.setIfNotNull(expenseInput.getComment(), expense::setComment);
    MethodUtils.setIfNotNull(expenseInput.getTitle(), expense::setTitle);
    return expenseRepository.save(expense);
  }
}
