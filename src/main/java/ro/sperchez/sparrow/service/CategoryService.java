package ro.sperchez.sparrow.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.Category;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.entity.input.CategoryInput;
import ro.sperchez.sparrow.exception.EntityNotFoundException;
import ro.sperchez.sparrow.repository.CategoryRepository;
import ro.sperchez.sparrow.repository.UserRepository;
import ro.sperchez.sparrow.util.Constants;
import ro.sperchez.sparrow.util.MethodUtils;

@Service
@Transactional
public class CategoryService {

  @Autowired private CategoryRepository categoryRepository;

  @Autowired private UserRepository userRepository;

  /**
   * Searches a category by ID and then returns it.
   *
   * @param id The UUID used for searching the category.
   * @return A category.
   */
  public Category getCategoryById(UUID id) {
    return categoryRepository.getById(id);
  }

  /**
   * Searches categories by user ID and then returns them.
   *
   * @param userId The user UUID used for searching the categories.
   * @return A list of categories.
   */
  public List<Category> findAllCategoriesByUserId(UUID userId) {
    return categoryRepository.findAllByUserId(userId);
  }

  /**
   * Searches categories by user ID and then returns them.
   *
   * @param userId The user UUID used for searching the categories.
   * @param pageable Used for pagination.
   * @return A list of categories.
   */
  public List<Category> findAllCategoriesByUserId(UUID userId, Pageable pageable) {
    return categoryRepository.findAllByUserId(userId, pageable);
  }

  /**
   * Gets the expenses count by user ID.
   *
   * @param userId The user UUID used for searching the expenses.
   * @return The expenses count.
   */
  public int getCategoryCountByUserId(UUID userId) {
    return categoryRepository.countByUserId(userId);
  }

  /**
   * Takes a CategoryInput to create a Category, and then returns it.
   *
   * @param categoryInput The category object.
   * @return A category object.
   */
  public Category createCategory(CategoryInput categoryInput) {
    Optional<User> user = userRepository.findById(categoryInput.getUserId());
    if (user.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    return categoryRepository.save(
        Category.builder()
            .id(UUID.randomUUID())
            .user(user.get())
            .iconName(categoryInput.getIconName())
            .name(categoryInput.getName())
            .color(categoryInput.getColor())
            .build());
  }

  /**
   * Deletes a category from the database.
   *
   * @param id The ID of the category to be deleted.
   */
  public void deleteCategoryById(UUID id) {
    categoryRepository.deleteById(id);
  }

  /**
   * Takes a CategoryInput to update a Category, and then returns it.
   *
   * @param categoryInput The category object.
   * @return A category object.
   */
  public Category updateCategory(CategoryInput categoryInput) {
    if (!categoryRepository.existsById(categoryInput.getId())) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.CATEGORY + Constants.ENTITY_NOT_FOUND_END,
          Constants.CATEGORY);
    }
    Category category = categoryRepository.getById(categoryInput.getId());
    if (!Objects.isNull(categoryInput.getUserId())) {
      Optional<User> user = userRepository.findById(categoryInput.getUserId());
      if (user.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
            Constants.USER);
      }
      MethodUtils.setIfNotNull(user.get(), category::setUser);
    }
    MethodUtils.setIfNotNull(categoryInput.getName(), category::setName);
    MethodUtils.setIfNotNull(categoryInput.getIconName(), category::setIconName);
    MethodUtils.setIfNotNull(categoryInput.getColor(), category::setColor);
    return categoryRepository.save(category);
  }
}
