package ro.sperchez.sparrow.service;

import java.util.Objects;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.User;
import ro.sperchez.sparrow.entity.input.GoogleUser;
import ro.sperchez.sparrow.entity.input.UserInput;
import ro.sperchez.sparrow.exception.EntityNotFoundException;
import ro.sperchez.sparrow.repository.UserRepository;
import ro.sperchez.sparrow.util.Constants;
import ro.sperchez.sparrow.util.MethodUtils;

@Service
@Transactional
public class UserService {

  @Autowired private UserRepository userRepository;

  @Autowired private SettingsService settingsService;

  @Autowired private BalanceService balanceService;

  @Autowired private AccountService accountService;

  /**
   * Searches a user by ID and then returns it.
   *
   * @param id The UUID used for searching the user.
   * @return A user.
   */
  public User getUserById(UUID id) {
    return userRepository.getById(id);
  }

  /**
   * Takes a UserInput to create a User, and then returns it.
   *
   * @param userInput The input object.
   * @return A user object.
   */
  public User createUser(UserInput userInput) {
    UUID userId = UUID.randomUUID();
    User user =
        userRepository.save(
            User.builder()
                .id(userId)
                .givenName(userInput.getGivenName())
                .familyName(userInput.getFamilyName())
                .email(userInput.getEmail())
                .build());
    settingsService.createSettingsForNewUser(user);
    balanceService.createBalanceForNewUser(user);
    accountService.createAccountForNewUser(user);
    return userRepository.getById(userId);
  }

  /**
   * Searches a user by Google sub, and if it doesn't exist, creates it.
   *
   * @param googleUser The GoogleUser object from the access token.
   * @return A user object.
   */
  public User findOrCreate(GoogleUser googleUser) {
    User user = userRepository.findBySub(googleUser.getSub());
    if (Objects.isNull(user)) {
      user =
          userRepository.save(
              User.builder()
                  .id(UUID.randomUUID())
                  .givenName(googleUser.getGivenName())
                  .familyName(googleUser.getFamilyName())
                  .email(googleUser.getEmail())
                  .sub(googleUser.getSub())
                  .profilePicture(googleUser.getProfilePicture())
                  .build());
      settingsService.createSettingsForNewUser(user);
      balanceService.createBalanceForNewUser(user);
      accountService.createAccountForNewUser(user);
    }
    return user;
  }

  /**
   * Deletes a user from the database.
   *
   * @param id The ID of the user to be deleted.
   */
  public void deleteUserById(UUID id) {
    userRepository.deleteById(id);
  }

  /**
   * Takes a UserInput to update a User, and then returns it.
   *
   * @param userInput The input object.
   * @return A user object.
   */
  public User updateUser(UserInput userInput) {
    if (!userRepository.existsById(userInput.getId())) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    User user = userRepository.getById(userInput.getId());
    MethodUtils.setIfNotNull(userInput.getEmail(), user::setEmail);
    MethodUtils.setIfNotNull(userInput.getGivenName(), user::setGivenName);
    MethodUtils.setIfNotNull(userInput.getFamilyName(), user::setFamilyName);
    return userRepository.save(user);
  }
}
