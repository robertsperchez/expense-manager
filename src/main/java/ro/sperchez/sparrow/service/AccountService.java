package ro.sperchez.sparrow.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.sperchez.sparrow.entity.*;
import ro.sperchez.sparrow.entity.input.AccountInput;
import ro.sperchez.sparrow.entity.input.ExpenseInput;
import ro.sperchez.sparrow.entity.input.IncomeInput;
import ro.sperchez.sparrow.exception.EntityNotFoundException;
import ro.sperchez.sparrow.repository.AccountRepository;
import ro.sperchez.sparrow.repository.BalanceRepository;
import ro.sperchez.sparrow.repository.UserRepository;
import ro.sperchez.sparrow.util.Constants;
import ro.sperchez.sparrow.util.MethodUtils;

@Service
@Transactional
public class AccountService {

  @Autowired private AccountRepository accountRepository;

  @Autowired private UserRepository userRepository;

  @Autowired private BalanceRepository balanceRepository;

  /**
   * Searches an account by ID and then returns it.
   *
   * @param id The UUID used for searching the account.
   * @return An account.
   */
  public Account getAccountById(UUID id) {
    return accountRepository.getById(id);
  }

  /**
   * Searches accounts by user ID and then returns them.
   *
   * @param userId The user UUID used for searching the accounts.
   * @return A list of accounts.
   */
  public List<Account> findAllAccountsByUserId(UUID userId) {
    return accountRepository.findAllByUserId(userId);
  }

  /**
   * Searches accounts by user ID and then returns them.
   *
   * @param userId The user UUID used for searching the accounts.
   * @param pageable Used for pagination.
   * @return A list of accounts.
   */
  public List<Account> findAllAccountsByUserId(UUID userId, Pageable pageable) {
    return accountRepository.findAllByUserId(userId, pageable);
  }

  /**
   * Takes an AccountInput to create an Account, and then returns it. It also updates the balance
   * for the user with the new account sum.
   *
   * @param accountInput The input object.
   * @return An account object.
   */
  public Account createAccount(AccountInput accountInput) {
    Optional<User> user = userRepository.findById(accountInput.getUserId());
    if (user.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
          Constants.USER);
    }
    Balance balance = user.get().getBalance();
    balance.setAmount(balance.getAmount() + accountInput.getAmount());
    balanceRepository.save(balance);
    return accountRepository.save(
        Account.builder()
            .id(UUID.randomUUID())
            .user(user.get())
            .amount(accountInput.getAmount())
            .name(accountInput.getName())
            .build());
  }

  /**
   * Takes a user to create an Account for that user, and then returns the account for it to be used
   * in the user creation.
   *
   * @param user The user that will be created with this account.
   * @return The account created.
   */
  public Account createAccountForNewUser(User user) {
    return accountRepository.save(
        Account.builder().id(UUID.randomUUID()).user(user).amount(0).name("Bank Account").build());
  }

  /**
   * Deletes an account from the database.
   *
   * @param id The ID of the account to be deleted.
   */
  public void deleteAccountById(UUID id) {
    Optional<Account> account = accountRepository.findById(id);
    if (account.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.ACCOUNT + Constants.ENTITY_NOT_FOUND_END,
          Constants.ACCOUNT);
    }
    Optional<Balance> balance =
        balanceRepository.findById(UUID.fromString(accountRepository.findUserIdByAccountId(id)));
    if (balance.isEmpty()) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.BALANCE + Constants.ENTITY_NOT_FOUND_END,
          Constants.BALANCE);
    }
    balance.get().setAmount(balance.get().getAmount() - account.get().getAmount());
    balanceRepository.save(balance.get());
    accountRepository.deleteById(id);
  }

  /**
   * Takes an AccountInput to update an Account, and then returns it. If the account sum changes, it
   * also updates the user's balance.
   *
   * @param accountInput The input object.
   * @return An account object.
   */
  public Account updateAccount(AccountInput accountInput) {
    if (!accountRepository.existsById(accountInput.getId())) {
      throw new EntityNotFoundException(
          Constants.ENTITY_NOT_FOUND_START + Constants.ACCOUNT + Constants.ENTITY_NOT_FOUND_END,
          Constants.ACCOUNT);
    }
    Account account = accountRepository.getById(accountInput.getId());
    if (!Objects.isNull(accountInput.getUserId())) {
      Optional<User> user = userRepository.findById(accountInput.getUserId());
      if (user.isEmpty()) {
        throw new EntityNotFoundException(
            Constants.ENTITY_NOT_FOUND_START + Constants.USER + Constants.ENTITY_NOT_FOUND_END,
            Constants.USER);
      }
      MethodUtils.setIfNotNull(user.get(), account::setUser);
    }
    if (Objects.nonNull(accountInput.getAmount())) {
      double updateSum = accountInput.getAmount() - account.getAmount();
      Balance balance = account.getUser().getBalance();
      balance.setAmount(balance.getAmount() + updateSum);
      balanceRepository.save(balance);
      account.setAmount(accountInput.getAmount());
    }
    MethodUtils.setIfNotNull(accountInput.getName(), account::setName);
    return accountRepository.save(account);
  }

  /**
   * Method used for updating the account and balance amounts for a user when he creates a new
   * income.
   *
   * @param income The saved database income object.
   * @param incomeInput The input used to update the income.
   */
  public void updateAccountAndBalanceForIncome(Income income, IncomeInput incomeInput) {
    if (Objects.isNull(incomeInput)) {
      Account account = income.getAccount();
      account.setAmount(account.getAmount() + income.getAmount());
      accountRepository.save(account);
      Balance balance = account.getUser().getBalance();
      balance.setAmount(balance.getAmount() + income.getAmount());
      balanceRepository.save(balance);
    } else if (Objects.nonNull(incomeInput.getAmount())
        && !incomeInput.getAmount().equals(income.getAmount())) {
      Account account = income.getAccount();
      account.setAmount(account.getAmount() + (incomeInput.getAmount() - income.getAmount()));
      accountRepository.save(account);
      Balance balance = account.getUser().getBalance();
      balance.setAmount(balance.getAmount() + (incomeInput.getAmount() - income.getAmount()));
      balanceRepository.save(balance);
    }
  }

  /**
   * Method used for updating the account and balance amounts for a user when he creates a new
   * expense.
   *
   * @param expense The saved database expense object.
   * @param expenseInput The input used to update the expense.
   */
  public void updateAccountAndBalanceForExpense(Expense expense, ExpenseInput expenseInput) {
    if (Objects.isNull(expenseInput)) {
      Account account = expense.getAccount();
      account.setAmount(account.getAmount() - expense.getAmount());
      accountRepository.save(account);
      Balance balance = account.getUser().getBalance();
      balance.setAmount(balance.getAmount() - expense.getAmount());
      balanceRepository.save(balance);
    } else if (Objects.nonNull(expenseInput.getAmount())
        && !expenseInput.getAmount().equals(expense.getAmount())) {
      Account account = expense.getAccount();
      account.setAmount(account.getAmount() - (expenseInput.getAmount() - expense.getAmount()));
      accountRepository.save(account);
      Balance balance = account.getUser().getBalance();
      balance.setAmount(balance.getAmount() - (expenseInput.getAmount() - expense.getAmount()));
      balanceRepository.save(balance);
    }
  }

  /**
   * Method used for updating the account and balance amounts for a user when he deletes an income.
   *
   * @param income The income that will be deleted.
   */
  public void updateAccountAndBalanceOnDeleteIncome(Income income) {
    Account account = income.getAccount();
    account.setAmount(account.getAmount() - income.getAmount());
    accountRepository.save(account);
    Balance balance = account.getUser().getBalance();
    balance.setAmount(balance.getAmount() - income.getAmount());
    balanceRepository.save(balance);
  }

  /**
   * Method used for updating the account and balance amounts for a user when he deletes an expense.
   *
   * @param expense The expense that will be deleted.
   */
  public void updateAccountAndBalanceOnDeleteExpense(Expense expense) {
    Account account = expense.getAccount();
    account.setAmount(account.getAmount() + expense.getAmount());
    accountRepository.save(account);
    Balance balance = account.getUser().getBalance();
    balance.setAmount(balance.getAmount() + expense.getAmount());
    balanceRepository.save(balance);
  }
}
